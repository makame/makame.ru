Blog = sequelize.define('blogs', {
  id: {
    type: Sequelize.INTEGER(11),
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: Sequelize.STRING,
    validate: {
      max: 100,
      min: 3,
    }
  },
  html: {
    type: Sequelize.TEXT,
    // allowNull: false,
    // validate: {
    //   notNull: true,
    //   max: 10000,
    //   min: 3,
    // },
    get: function() {
      var value = this.getDataValue('html')
      try
      {
        value = JSON.parse(value)
      }
      catch (e)
      {
        value = []
      }
      return value
    },
    set: function(value) {
      this.setDataValue('html', JSON.stringify(value));
    },
  },
  preview: {
    type: Sequelize.TEXT,
    get: function() {
      var value = this.getDataValue('preview')
      try
      {
        value = JSON.parse(value)
      }
      catch (e)
      {
        value = []
      }
      return value
    },
    set: function(value) {
      this.setDataValue('preview', JSON.stringify(value));
    },
  },
  date: {
    type: Sequelize.DATE,
  },
  icon: {
    type: Sequelize.STRING,
    validate: {
      max: 10,
      min: 1,
    }
  },
  image: {
    type: Sequelize.STRING,
    validate: {
      max: 1000,
      min: 3,
    }
  },
}, {
  timestamps: false,
  paranoid: false,
  underscored: true,
  freezeTableName: true,
  tableName: 'blogs'
});
