User = sequelize.define('users', {
  id: {
    type: Sequelize.INTEGER(11),
    primaryKey: true,
  },
  username: {
    type: Sequelize.STRING,
    // validate: {
    //   max: 20,
    //   min: 0,
    // }
  },
  date: {
    type: Sequelize.DATE,
  },
  access_token: {
    type: Sequelize.TEXT,
    // allowNull: false,
    // validate: {
    //   notNull: true,
    //   max: 50,
    //   min: 1,
    // }
  },
  // avatar: {
  //   type: Sequelize.STRING,
  //   // validate: {
  //   //   max: 100,
  //   //   min: 3,
  //   // }
  // },
  facebook: {
    type: Sequelize.STRING,
    // validate: {
    //   max: 50,
    //   min: 0,
    // }
  },
  roles: {
    type: Sequelize.STRING,
    // validate: {
    //   max: 50,
    //   min: 0,
    // }
    get: function() {
      var roles = this.getDataValue('roles')
      try
      {
        roles = JSON.parse(roles)
      }
      catch (e)
      {
        roles = []
      }
      return roles
    },
    set: function(value) {
      this.setDataValue('roles', JSON.stringify(value));
    },
  },
}, {
  timestamps: false,
  paranoid: false,
  underscored: true,
  freezeTableName: true,
  tableName: 'users',
  // getterMethods: {
  //   rolesArray: function () {
  //     return JSON.parse(this.getDataValue('roles'))
  //   }
  // },
  // setterMethods: {
  //   rolesArray: function (value) {
  //     this.setDataValue('roles', JSON.stringify(value));
  //   }
  // }
});
