import React from 'react'
import ReactDOM from 'react-dom'
import Link from '../Controls/Link.jsx'
import $ from 'jquery';
import Velocity from 'velocity-animate';
import _ from 'underscore';
import RouteChanger from '../Controls/RouteChanger.jsx'
import Helper from '../Common/Helper.jsx'

export default React.createClass({

  // contextTypes: {
  //   blogs: React.PropTypes.object,
  // },
  //
  // getContext: function() {
  //   return {
  //     blogs: ['hello! Test']
  //   }
  // },

  getInitialState: function(){
    const self = this

    return {
      blogs: {
        list: [],
        count: 0,

        limit: 0,
        per: 4,

        initBlog: function() {
          self.state.blogs.list = []
          self.state.blogs.limit = 0
          self.state.blogs.per = 4
        },

        initCreateBlog() {
          self.state.blogs.list.push(
            {
              id: -1,
              title: '',
              image: '',
              date: new Date(),
              icon: '',
              showed: false,
              preview: [],
              html: [],
              previewOld: [],
              htmlOld: [],
            }
          )

          self.setState({blogs: self.state.blogs})
        },

        loadBlog: function(id, callback, showed) {
          const dataReq = {
            id: id,
          }

          $.ajax({
            type: 'POST',
            url: '/api/get_blog',
            data: dataReq
          })
          .done(function(data) {
            const item = JSON.parse(data)

            var find = _.find(self.state.blogs.list, function(kit){return item.id == kit.id})
            if (find) {
              find.title = item.title
              find.image = item.image
              find.date = new Date(Date.parse(item.date))
              find.icon = item.icon
              find.preview = item.preview
              find.html = item.html
              find.showed = showed
              find.previewOld = $.extend(true, {}, item.preview)
              find.htmlOld = $.extend(true, {}, item.html)
            }
            else {
              self.state.blogs.list.push(
                {
                  id: item.id,
                  title: item.title,
                  image: item.image,
                  date: new Date(Date.parse(item.date)),
                  icon: item.icon,
                  showed: showed,
                  preview: item.preview,
                  html: item.html,
                  previewOld: $.extend(true, {}, item.preview),
                  htmlOld: $.extend(true, {}, item.html),
                }
              )
            }

            if (callback) {
              callback(item, null)
            }

            self.setState({blogs: self.state.blogs})
          })
          .fail(function(error) {
            console.log(error);
            if (callback) {
              callback(null, error)
            }
          });
        },

        setIcon: function(id) {
          return function() {
            var find = _.find(self.state.blogs.list, function(kit){return kit.id == id})
            find.icon = window.prompt("Enter Icon URL", find.icon);
            self.setState({blogs: self.state.blogs});
          }
        },

        setImage: function(id) {
          return function() {
            var find = _.find(self.state.blogs.list, function(kit){return kit.id == id})
            find.image = window.prompt("Enter Image URL", find.image);
            self.setState({blogs: self.state.blogs});
          }
        },

        linkValue: function(id, value) {
          var find = _.find(self.state.blogs.list, function(kit){return kit.id == id})
          return {
            value: find[value],
            requestChange: function(newValue) {
              // console.log(newValue[0][0]);
              find[value] = newValue;
              // console.log(self.state.blogs.list[index].htmlLink.value[0][0]);
              self.setState({blogs: self.state.blogs});
            }
          }
        },

        loadMore: function(callback) {
          const dataReq = {
            limit: self.state.blogs.per,
            skip: self.state.blogs.limit,
          }

          $.ajax({
            type: 'POST',
            url: '/api/get_blogs',
            data: dataReq
          })
          .done(function(data) {
            data = JSON.parse(data)

            _.each(data.items, function(item) {
              var find = _.find(self.state.blogs.list, function(kit){return item.id == kit.id})
              if (find) {
                find.title = item.title
                find.image = item.image
                find.date = new Date(Date.parse(item.date))
                find.image = item.image
                find.icon = item.icon
                find.preview = item.preview
              }
              else {
                self.state.blogs.list.push(
                  {
                    id: item.id,
                    title: item.title,
                    image: item.image,
                    date: new Date(Date.parse(item.date)),
                    icon: item.icon,
                    showed: false,
                    preview: item.preview,
                  }
                )
              }
            })

            if (callback) {
              callback()
            }

            self.state.blogs.limit = self.state.blogs.limit + self.state.blogs.per
            self.state.blogs.count = data.count

            self.setState({blogs: self.state.blogs})
          })
          .fail(function(error) {
            console.log(error);
            if (callback) {
              callback(error)
            }
          });
        },

        saveBlog: function(id, callback) {
          var find = _.find(self.state.blogs.list, function(kit){return id == kit.id})

          if (!find) {
            callback(null, "No surch id " + id)
            throw new Error("No surch id " + id)
          }

          const dataReq = {
            id: find.id,
            title: find.title,
            image: find.image,
            date: find.date,
            icon: find.icon,
            preview: find.preview,
            html: find.html,
          }

          $.ajax({
            type: 'POST',
            url: '/api/upsert_blog',
            data: dataReq
          })
          .done(function(data) {
            const item = JSON.parse(data)

            if (callback) {
              callback(item, null)
            }

            // self.setState({blogs: self.state.blogs})
          })
          .fail(function(error) {
            console.log(error);
            if (callback) {
              callback(null, error)
            }
          });
        },

        removeBlog: function(id, callback) {
          var find = _.find(self.state.blogs.list, function(kit){return id == kit.id})

          if (!find) {
            callback(null, "No surch id " + id)
            throw new Error("No surch id " + id)
          }

          const dataReq = {
            id: find.id,
          }

          $.ajax({
            type: 'POST',
            url: '/api/remove_blog',
            data: dataReq
          })
          .done(function(data) {
            const item = JSON.parse(data)

            if (callback) {
              callback(item, null)
            }
          })
          .fail(function(error) {
            console.log(error);
            if (callback) {
              callback(null, error)
            }
          });
        },
      },
    };
  },

  childContextTypes: {
    blogs: React.PropTypes.object
  },

  getChildContext: function() {
    return {
      blogs: this.state.blogs
    }
  },

  render() {
    return (
      <div className="container align-center margin-top-5">
        <div className="container align-center title medium padding-bottom-3"><span>Blog</span></div>
        <div className="container align-center title"><span>I show you</span></div>
        <div className="container align-center title small"><span>#Design #Web #Desktop #Programmer</span></div>

        <div className="inlines container be center align-center margin-top-2 text thin">
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://www.facebook.com/profile.php?id=100001115479981"><span className="fa fa-facebook"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="skype:makamekm?chat"><span className="fa fa-skype"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://twitter.com/makame_"><span className="fa fa-twitter"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://bitbucket.org/makame/"><span className="fa fa-bitbucket"></span></a>
          </div>
        </div>

        <div className="container white margin-top-6 padding-vertical-5">
          <div className="container center be align-left padding-horizontal-2">

            {this.props.children.props.children}

          </div>
        </div>

        <div className="container align-center title margin-top-6"><span>I believe in you</span></div>
        <div className="container align-center title small"><span>#Believe #YouCan #You</span></div>

        <div className="container align-center title small padding-bottom-2 margin-top-6"><span>makame#2016</span></div>
      </div>
    )
  }
})
