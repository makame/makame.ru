import React from 'react'
import ReactDOM from 'react-dom'
import { PaneContainer, Pane } from '../Controls/PaneContainer.jsx'
import _ from 'underscore'
import PopupContainer from '../Controls/PopupContainer.jsx'
import Popup from '../Controls/Popup.jsx'
import PaneRouteChanger from '../Controls/PaneRouteChanger.jsx'
import Velocity from 'velocity-animate'
import $ from 'jquery'

export default React.createClass({
  scrollTop(offset) {
    $(document).scrollTop(offset)
    // Velocity(ReactDOM.findDOMNode(this.refs.content), "scroll", { duration: 300, offset: offset })
  },

  scrollLeft(offset) {
    $(document).scrollLeft(offset)
    // Velocity(ReactDOM.findDOMNode(this.refs.content), "scroll", { duration: 300, offset: offset })
  },

  scrollContainer() {
    return $('body')
    // Velocity(ReactDOM.findDOMNode(this.refs.content), "scroll", { duration: 300, offset: offset })
  },

  render() {
    return (
      <PopupContainer>
        <div className="left-menu">
          <div className="header">
            Ad
          </div>
          <Popup className="item" position="right" popupClassName="auto" delay={0} activeClassName="" html={(<div className="padding-vertical-1 padding-horizontal-d2 text sub mini upper"><span className="fa fa-home padding-right-d1"></span>Home</div>)}>
            <span className="fa fa-home"></span>
          </Popup>
          <Popup className="item" position="right" popupClassName="auto" delay={0} activeClassName="" html={(<div className="padding-vertical-1 padding-horizontal-d2 text sub mini upper"><span className="fa fa-inbox padding-right-d1"></span>Inbox</div>)}>
            <span className="fa fa-inbox"></span>
          </Popup>
          <Popup className="item" position="right" popupClassName="auto" delay={0} activeClassName="" html={(<div className="padding-vertical-1 padding-horizontal-d2 text sub mini upper"><span className="fa fa-terminal padding-right-d1"></span>Terminal</div>)}>
            <span className="fa fa-terminal"></span>
          </Popup>
        </div>
        <div className="margin-left-5" ref="content">
          <PaneRouteChanger scrollTop={this.scrollTop} scrollLeft={this.scrollLeft} scrollContainer={this.scrollContainer} location={this.props.location}>
            {this.props.children}
          </PaneRouteChanger>
        </div>
      </PopupContainer>
    )
  }
})
