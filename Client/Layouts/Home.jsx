import React from 'react'
import ReactDOM from 'react-dom'
import RouteChanger from '../Controls/RouteChanger.jsx'
import Link from '../Controls/Link.jsx'
import $ from 'jquery';
import Velocity from 'velocity-animate';
import Swipeable from 'react-swipeable';
import PopupContainer from '../Controls/PopupContainer.jsx'

export default React.createClass({
  childContextTypes: {
    user: React.PropTypes.object,
    is_loggining: React.PropTypes.bool,
  },

  getChildContext: function() {
    return {
      user: this.state.user,
      is_loggining: this.state.is_loggining,
    }
  },

  getInitialState: function(){
    return {
      user: null,
      is_loggining: true,
    };
  },

  componentWillMount() {
    const self = this

    self.setState({is_loggining: true})

    $.ajax({
      type: 'POST',
      url: '/api/get_self'
    })
    .done(function(data) {
      const item = JSON.parse(data)

      self.state.user = item

      self.setState({user: self.state.user, is_loggining: false})
    })
    .fail(function(error) {
      self.setState({is_loggining: false})
    });
  },

  hide() {
    var el = ReactDOM.findDOMNode(this.refs.menu);
    var showButton = ReactDOM.findDOMNode(this.refs.showButton);
    var hideButton = ReactDOM.findDOMNode(this.refs.hideButton);
    // $(el).hide()
    Velocity(el, { opacity: 0.0, marginLeft: '-70vw' }, { display: "none", duration: 150, easing: "easeInSine" })
    Velocity(showButton, { scale: 0, opacity: 0 }, { duration: 0 })
    Velocity(showButton, { scale: 1, opacity: 1 }, { display: "", duration: 150 / 2, delay: 150 / 2, easing: "easeInSine" })
    Velocity(hideButton, { scale: 1, opacity: 1 }, { duration: 0 })
    Velocity(hideButton, { scale: 0, opacity: 0 }, { display: "none", duration: 150 / 2, easing: "easeInSine" })

    var childs = $(el).children()
    Velocity(childs, { opacity: 0, scale: 0.9 }, { duration: 150, delay: 50, easing: "easeInSine" })
  },

  show(e) {
    var el = ReactDOM.findDOMNode(this.refs.menu);
    var showButton = ReactDOM.findDOMNode(this.refs.showButton);
    var hideButton = ReactDOM.findDOMNode(this.refs.hideButton);
    // $(el).show()
    $(el).hide()
    Velocity(el, { opacity: 1.0, marginLeft: 0 }, { display: "block", duration: 150, easing: "easeInSine" })
    Velocity(showButton, { scale: 1, opacity: 1 }, { duration: 0 })
    Velocity(showButton, { scale: 0, opacity: 0 }, { display: "none", duration: 150 / 2, easing: "easeInSine" })
    Velocity(hideButton, { scale: 0, opacity: 0 }, { duration: 0 })
    Velocity(hideButton, { scale: 1, opacity: 1 }, { display: "", duration: 150 / 2, delay: 150 / 2, easing: "easeInSine" })

    var childs = $(el).children()
    // $(childs).hide()
    // Velocity(childs, { opacity: 0, scale: 0.9, display: "none" }, { duration: 0 })
    childs.hide()
    // Velocity(el, { opacity: 0.0, scale: 1.0 }, { display: "none", duration: 0 })
    Velocity(childs, { opacity: 1.0, scale: 1 }, { display: "none", display: "", duration: 150, delay: 50, easing: "easeInSine" })
    // Velocity(childs, { opacity: 1, scale: 1, display: "" }, { display: "none", duration: 150, delay: 50, easing: "easeInSine" })
  },

  render() {
    var avatar = (
      <a href={'/login'} className="nota item-avatar" onClick={function(e){$(e.target).addClass('loading')}}><img src="../../../facebook-squarelogo.png"></img></a>
    )
    if (this.state.is_loggining) {
      avatar = (
        <a className="nota item-avatar loading"><img src="../../../facebook-squarelogo.png"></img></a>
      )
    }
    else if (this.state.user) {
      avatar = (
        <a href={'/logout'} className="nota item-avatar" onClick={function(e){$(e.target).addClass('loading')}}><img src={"http://graph.facebook.com/" + this.state.user.facebook + "/picture?type=square"}></img></a>
      )
    }
    return (
      <PopupContainer>
        <div className="stars-container">
          <div id="stars"></div>
          <div id="stars2"></div>
          <div id="stars3"></div>
        </div>
        <Swipeable onSwipedLeft={this.hide} onSwipedRight={this.show}>
          <div>
            <div className="top-menu container center be margin-top-3">
              <div className="items-desktop">
                <Link to={'/hello'} className="item" activeClassName="active">Hello</Link>
                <Link to={'/blog'} className="item" activeClassName="active">Blog</Link>
                <Link to={'/resume'} className="item" activeClassName="active">Resume</Link>
                <Link to={'/prices'} className="item" activeClassName="active">Prices</Link>
                <Link to={'/admin'} className="item" activeClassName="active">Admin</Link>
              </div>
              <div className="items-mobile">
                <a className="item" onClick={this.show}><span className="fa fa-reorder"></span></a>
                {avatar}
              </div>
              <div className="space"></div>
              <div className="items-desktop">
                {avatar}
              </div>
              <Link to={'/contacts'} ref="showButton" className="item" activeClassName="active" style={{willChange: 'opacity, transform'}}>Contacts</Link>
              <a className="item" ref="hideButton" style={{display: 'none', scale: 0, willChange: 'opacity, transform'}} onClick={this.hide}><span className="fa fa-chevron-left"></span></a>
            </div>

            <RouteChanger duration={1000} location={this.props.location}>
              {this.props.children}
            </RouteChanger>
          </div>

          <div className="mobile-menu" ref="menu" style={{width: "70vw", marginLeft: "-70vw", willChange: 'opacity, margin'}}>
            <Link to={'/hello'} className="item margin-top-3" activeClassName="active" onClick={this.hide}>Hello</Link>
            <Link to={'/blog'} className="item" activeClassName="active" onClick={this.hide}>Blog</Link>
            <Link to={'/resume'} className="item" activeClassName="active" onClick={this.hide}>Resume</Link>
            <Link to={'/prices'} className="item" activeClassName="active" onClick={this.hide}>Prices</Link>
            <Link to={'/contacts'} className="item" activeClassName="active" onClick={this.hide}>Contacts</Link>
          </div>
        </Swipeable>
      </PopupContainer>
    )
  }
})
