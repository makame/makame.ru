import React from 'react'
import ReactDOM from 'react-dom'
import Link from '../Controls/Link.jsx'
import $ from 'jquery';
import Velocity from 'velocity-animate';
import _ from 'underscore';
import { browserHistory } from 'react-router'
import StdAnim from '../Controls/StdAnim.jsx'
import Config from '../../Config/Public_Config.js';
import CommentsCount from '../Controls/Facebook/CommentsCount.jsx'
import Editor from '../Controls/Editor.jsx';
import FacebookLike from '../Controls/Facebook/Like.jsx'
import FacebookShare from '../Controls/Facebook/Share.jsx'

export default React.createClass({
  contextTypes: {
    blogs: React.PropTypes.object,
    user: React.PropTypes.object,
    is_loggining: React.PropTypes.bool,
    set_loading: React.PropTypes.func,
    unset_loading: React.PropTypes.func,
  },

  // getInitialState() {
  //   return {
  //     blogs: [],
  //     limit: 10,
  //   }
  // },

  loadMore(e) {
    const self = this
    const button = $(e.target)
    button.addClass('loading')
    self.context.blogs.loadMore(function() {
      button.removeClass('loading')
    })
  },

  componentDidMount() {
    const self = this

    self.context.set_loading()
    self.context.blogs.initBlog()
    self.context.blogs.loadMore(function() {
      self.context.unset_loading()
    })
  },

  getFormattedDate(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
  },

  render() {
    const self = this

    var blogs = [];

    _.each(this.context.blogs.list, function(blog) {

      var open = function(e) {
        const button = $(e.target)
        button.addClass('loading')
        self.context.blogs.loadBlog(blog.id, function() {
          button.removeClass('loading')
          browserHistory.push('/blog/' + blog.id)
        })
      }

      var show = function(e) {
        const button = $(e.target)
        $(e.target).addClass('loading')
        self.context.blogs.loadBlog(blog.id, function() {
          button.removeClass('loading')
        }, true)
      }

      var classNameBlog = "blog margin-top-4"
      var dataHtml = (
        <Editor className="padding-d1 margin-top-1 text thin" placeholder="Enter Preview" ref="editor" linkState={self.context.blogs.linkValue(blog.id, 'preview')} disabled={true}></Editor>
      )

      if (!blog.showed) {
        classNameBlog += " off"
      }
      else {
        dataHtml = (
          <Editor className="padding-d1 margin-top-1 text thin" placeholder="Enter Preview" ref="editor" linkState={self.context.blogs.linkValue(blog.id, 'html')} disabled={true}></Editor>
        )
      }

      blogs.push(
        <div className={classNameBlog} ref="blog" key={blog.id}>
          <h3 className="text upper thin align-left margin-0 link" onClick={open}>{blog.icon.trim() == "" ? null : (<span className={"fa fa-" + blog.icon}></span>)}  {blog.title}</h3>

          <div className="margin-top-3 blog-image">
            <img className="" src={blog.image.trim() == "" ? "../../../Images/nf.jpg" : blog.image}></img>

            <div className="button-area padding-right-d1 padding-bottom-d1 align-right">
              <a className="button mini semitransparent" onClick={open}><span className="fa fa-angle-right"></span>OPEN</a>
            </div>
          </div>

          <div className="padding-top-1 blog-container padding-bottom-1">

            <div className="inlines text thin align-left padding-left-d1 padding-top-1">
              <div className="padding-right-1">Posted: <span className="text sub">{self.getFormattedDate(blog.date)}</span></div>
            </div>

            <div className="inlines text thin align-left padding-left-d1 padding-top-1">
              <div className="padding-right-1">Comments: <CommentsCount href={Config.address + "/blog/" + blog.id}/></div>
            </div>

            <div className="inlines text thin align-left padding-left-d1 padding-top-1">
              <FacebookLike href={"http://" + Config.address + "/share/blog/" + blog.id}/>
            </div>

            <div className="inlines text thin align-left padding-left-d1 padding-top-1">
              <FacebookShare href={"http://" + Config.address + "/share/blog/" + blog.id}/>
            </div>

            {dataHtml}

            <div className="blog-button" onClick={show}>
              <div><span className="fa fa-angle-down"></span> Show All <span className="fa fa-angle-down"></span></div>
            </div>

          </div>
        </div>
      )
    })

    var loadMore = null

    if (self.context.blogs.limit < self.context.blogs.count) {
      loadMore = (
        <div className="margin-top-4 align-center">
          <div className="button transparent" onClick={this.loadMore}>
            <span className="fa fa-angle-down"></span> Load More <span className="fa fa-angle-down"></span>
          </div>
        </div>
      )
    }

    return (
      <div>
        <h2 className="text upper thin align-center margin-0">What do you want from me?</h2>

        <StdAnim duration={500}>
          {blogs}
        </StdAnim>

        {loadMore}

      </div>
    )
  }
})
