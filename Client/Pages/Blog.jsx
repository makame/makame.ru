import React from 'react'
import ReactDOM from 'react-dom'
import Link from '../Controls/Link.jsx'
import $ from 'jquery';
import Velocity from 'velocity-animate';
import _ from 'underscore';
import Config from '../../Config/Public_Config.js';
import Editor from '../Controls/Editor.jsx';
import EditorSimple from '../Controls/EditorSimple.jsx';
import { browserHistory } from 'react-router'
import FacebookComments from '../Controls/Facebook/Comments.jsx'
import FacebookLike from '../Controls/Facebook/Like.jsx'
import FacebookShare from '../Controls/Facebook/Share.jsx'
import Select from '../Controls/Select.jsx';

export default React.createClass({

  contextTypes: {
    blogs: React.PropTypes.object,
    user: React.PropTypes.object,
    is_loggining: React.PropTypes.bool,
    set_loading: React.PropTypes.func,
    unset_loading: React.PropTypes.func,
  },

  componentDidMount() {
    const self = this

    if (self.props.params.blogId == -1) {
      self.context.blogs.initBlog()
      self.context.blogs.initCreateBlog()
    }
    else {
      self.context.set_loading()
      self.context.blogs.initBlog()
      self.context.blogs.loadBlog(self.props.params.blogId, function() {
        self.context.unset_loading()
      })
    }
  },

  saveBlog(e) {
    const self = this
    const button = $(e.target)
    button.addClass('loading')
    self.context.blogs.saveBlog(self.props.params.blogId, function(result, error) {
      if (error) {
        button.removeClass('loading')
      }
      else if (result) {
        console.log();
        self.context.blogs.loadBlog(result.id, function() {
          button.removeClass('loading')
          browserHistory.push('/blog/' + result.id)
        })
      }
      else {
        button.removeClass('loading')
      }
    })
  },

  removeBlog(e) {
    const self = this
    const button = $(e.target)
    var ask = window.confirm("Do you want to remove this Blog?");
    if (ask) {
      button.addClass('loading')
      self.context.blogs.removeBlog(self.props.params.blogId, function(result, error) {
        if (error) {
          button.removeClass('loading')
        }
        else {
          button.removeClass('loading')
          browserHistory.push('/blog/')
        }
      })
    }
  },

  getInitialState() {
    return {
      preview: false,
    }
  },

  toggleView() {
    this.setState({preview: !this.state.preview})
  },

  getFormattedDate(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
  },

  render() {
    const self = this

    var blog = _.find(this.context.blogs.list, function(item) {
      return item.id == self.props.params.blogId
    })

    if (!blog) {
      return null
    }

    const editable = self.props.params.blogId == -1 ? true : (self.context.user ? _.contains(self.context.user.roles, 'admin') : false)
    // const editable = true

    var saveButtons = []
    if (editable) {
      if (this.state.preview) {
        saveButtons.push(
          <div className="button semitransparent mini" onClick={this.toggleView} key="view">Preview</div>
        )
      }
      else {
        saveButtons.push(
          <div className="button semitransparent mini" onClick={this.toggleView} key="view">Full</div>
        )
      }
      saveButtons.push(
        <div className="button semitransparent mini margin-left-1" onClick={this.context.blogs.setImage(blog.id)} key="image"><span className="fa fa-image"></span> Image</div>
      )
      saveButtons.push(
        <div className="button semitransparent mini margin-left-1" onClick={this.context.blogs.setIcon(blog.id)} key="icon"><span className="fa fa-asterisk"></span> Icon</div>
      )
      saveButtons.push(
        <div className="button red mini margin-left-1" onClick={this.removeBlog} key="remove"><span className="fa fa-trash"></span> Remove</div>
      )
      saveButtons.push(
        <div className="button blue mini margin-left-1" onClick={this.saveBlog} key="save"><span className="fa fa-save"></span> Save</div>
      )
    }

    var editor = (
      <Editor className="padding-d1 margin-top-1 text thin" placeholder="Enter Text" ref="editor" reference={blog.htmlOld} linkState={this.context.blogs.linkValue(blog.id, 'html')} disabled={!editable}></Editor>
    )

    if (this.state.preview) {
      editor = (
        <Editor className="padding-d1 margin-top-1 text thin" placeholder="Enter Preview" ref="editor" reference={blog.previewOld} linkState={this.context.blogs.linkValue(blog.id, 'preview')} disabled={!editable}></Editor>
      )
    }

    return (
      <div>
        <div className="blog" ref="blog" key={blog.id}>
          <h3 className="text upper thin align-left margin-0">{blog.icon.trim() == "" ? null : (<span className={"fa fa-" + blog.icon}></span>)} <EditorSimple placeholder="Enter Title" linkState={this.context.blogs.linkValue(blog.id, 'title')} disabled={!editable}></EditorSimple></h3>

          <div className="margin-top-3 blog-image">
            <img className="" src={blog.image.trim() == "" ? "../../../Images/nf.jpg" : blog.image}></img>

            <div className="button-area padding-right-d1 padding-bottom-d1 align-right" key="image">
              {saveButtons}
            </div>
          </div>

          <div className="padding-top-1 blog-container padding-bottom-1">

            <div className="inlines text thin align-left padding-left-d1 padding-top-1">
              <div className="padding-right-1">Posted: <span className="text sub">{this.getFormattedDate(blog.date)}</span></div>
            </div>

            {self.props.params.blogId == -1 ? null : (<div className="inlines text thin align-left padding-left-d1 padding-top-1">
              <FacebookLike href={"http://" + Config.address + "/share/blog/" + self.props.params.blogId}/>
            </div>)}

            {self.props.params.blogId == -1 ? null : (<div className="inlines text thin align-left padding-left-d1 padding-top-1">
              <FacebookShare href={"http://" + Config.address + "/share/blog/" + self.props.params.blogId}/>
            </div>)}

            <div className="padding-vertical-2"><Select/></div>

            {editor}

          </div>

          {self.props.params.blogId == -1 ? null : (<div className="padding-top-1 padding-bottom-1 container center align-center">
            <FacebookComments href={Config.address + "/blog/" + self.props.params.blogId}/>
          </div>)}
        </div>

      </div>
    )
  }
})
