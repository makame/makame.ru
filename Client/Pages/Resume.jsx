import React from 'react'
import ReactDOM from 'react-dom'
import Link from '../Controls/Link.jsx'
import $ from 'jquery';
import Velocity from 'velocity-animate';

export default React.createClass({

  render() {
    return (
      <div className="container align-center margin-top-5">
        <div className="container align-center title medium padding-bottom-3"><span>Resume</span></div>
        <div className="container align-center title"><span>I will talk with you</span></div>
        <div className="container align-center title small"><span>#Design #Web #Desktop #Programmer</span></div>

        <div className="inlines container be center align-center margin-top-2 text thin">
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://www.facebook.com/profile.php?id=100001115479981"><span className="fa fa-facebook"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="skype:makamekm?chat"><span className="fa fa-skype"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://twitter.com/makame_"><span className="fa fa-twitter"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://bitbucket.org/makame/"><span className="fa fa-bitbucket"></span></a>
          </div>
        </div>

        <div className="container white margin-top-6 padding-vertical-5">
          <div className="container center be align-left padding-horizontal-2">
            <h2 className="text upper thin align-center margin-0">What do you want from me?</h2>

            <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-cloud"></span> First of All</h3>

            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text thin">Position / Main expertise:</div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">Web Development (Backend & Frontend), Desktop Application</div>
            </div>

            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text thin">IT experience:</div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">Since 2007 (9 years)</div>
            </div>

            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text thin">Education:</div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">2009-2015, BMSTU, specialist (Programmer & Laser Technology Engineer)</div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">2016-2018, MTU, magist (Programmer)</div>
            </div>

            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text thin">Summary:</div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">(general information about the achievements of the developer 5-6 sentences)</div>
            </div>


            <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-cloud"></span> Technical skills</h3>

            <div className="inlines should-2 mobile-1 margin-top-3">

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin">Programming Languages/ Technologies</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">C, C#, C++ (.NET)</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">PHP</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">JavaScript, JSX</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">CSS, SCSS, SASS, Less</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Java</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">ASP, ASP MVC</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">NodeJS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Swift</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Python</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">ReqExp</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Ruby</div>
              </div>


              <div className="align-right padding-horizontal-d1 margin-top-3">
                <div className="text thin">RDBMS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Oracle</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MySQL</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MSSQL</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">PostgreSQL</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MongoDB</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">RethinkDB</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Redis</div>
              </div>


              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin">Frameworks</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">jQuery</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Ext JS / Sencha</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Bootstrap</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Foundation</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Semantic UI</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Underscore</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Cocoa via Swift</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Meteor JS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Django</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Cocoa via Swift</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">React JS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Angular JS 1 & 2</div>
              </div>


              <div className="align-right padding-horizontal-d1 margin-top-3">
                <div className="text thin">Methodologies</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">OOP/SOLID</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">TDD</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">PCI DSS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">KISS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">DRY</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Reactivity</div>
              </div>


              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin">Development Tools</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Eclipse</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">NetBeans</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">IntelliJ IDEA</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">GIT / SVN</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Trello</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">JIRA</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">TODO List</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">XCode</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Atom</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MS Visual Studio</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Xamarin</div>
              </div>


              <div className="align-right padding-horizontal-d1 margin-top-3">
                <div className="text thin">Testing Tools</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">RobotFramework</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MS Unit Testing</div>
              </div>


              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin">Operating Systems</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Mac OS X</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Linux / FreeBSD</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Microsoft Windows</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MS DOS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Chrome OS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Android</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">IOS</div>
              </div>


              <div className="align-right padding-horizontal-d1 margin-top-3">
                <div className="text thin">Languages</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Russian (native)</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">English (intermediate)</div>
              </div>

            </div>

          </div>
        </div>

        <div className="container align-center title margin-top-6"><span>I can think of</span></div>
        <div className="container align-center title small"><span>#Idea #Live #Thinkof</span></div>

        <div className="container white margin-top-6 padding-vertical-5">
          <div className="container center be align-left padding-horizontal-2">
            <h2 className="text upper thin align-center margin-0">What do you want from me?</h2>

            <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-cloud"></span> Project experience (THE PROJECT UNDER NDA)</h3>

            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#0 <span className="text thin">Project name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">BMSTU Experiments and Government Orders</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(March 2010 - February 2013, 36 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Customer:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">BMSTU</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project role:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Laser Technology Engineer 2nd category</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project team size:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">9 Person</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Private Social Network</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Tools and technologies:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">HTML, PHP, Photoshop, AutoCad, Kompas 3D, 3DMax, Modo, ZBrush, C++, C#</div>
              </div>
            </div>


            <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-cloud"></span> Project experience (THE PROJECT NOT UNDER NDA)</h3>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#1 <span className="text thin">Project name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">Custom Website Design</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(Autumn 2007 - Autumn 2009, 24 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Customer:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Private enterprises, Veterinary, 1C, etc.</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project role:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Single FullStack Developer</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project team size:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Single | + Designer</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Commercial Sites & Desktop Applications</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Tools and technologies:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Flash, Notepad, PHP, HTML, Photoshop, C++</div>
              </div>
            </div>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#2 <span className="text thin">Project name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">BMSTU MT12 Site</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(January 2010 - March 2010, 3 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Customer:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">BMSTU</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project role:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Programmer</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project team size:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">2 (Programmer, Designer)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Commercial Site</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Tools and technologies:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Notepad, HTML, PHP, Photoshop</div>
              </div>
            </div>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#3 <span className="text thin">Project name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">MONT Web Site (Part for Vendor)</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(May 2014 - June 2015, 1 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Customer:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MONT</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project role:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Backend Programmer</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project team size:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">20</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Development of the Supplier (www.mont.com)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Tools and technologies:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">ASP.NET MVC</div>
              </div>
            </div>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#4 <span className="text thin">Project name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">MONT Internal Web Interface</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(June 2014 - August 2015, 2 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Customer:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MONT</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project role:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">FullStack Programmer</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project team size:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">4</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">The internal system for communication employees</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Tools and technologies:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">ASP.NET MVC, SharePoint</div>
              </div>
            </div>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#5 <span className="text thin">Project name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">MONT System for Processing & OCR & Storage Document</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(August 2014 - December 2015, 17 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Customer:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MONT</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project role:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Programmer</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project team size:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">8</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">The internal system for processing, OCR, storage</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Tools and technologies:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Abbyy Flexi Capture, ASP.NET, C#</div>
              </div>
            </div>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#6 <span className="text thin">Project name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">MONT Internal Logistic System (Sheriff)</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(June 2014 - February 2016, 21 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Customer:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MONT</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project role:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Programmer (Desktop & Server)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project team size:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">5 (about 300 users)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">The internal logistic system for processing orders automatically, tracking customer subscriptions, as well as physical products</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Tools and technologies:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">C# (.NET), MSSQL</div>
              </div>
            </div>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#7 <span className="text thin">Project name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">Projector</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(December 2014 - January 2015, 2 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Customer:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Startup “Projector”</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project role:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Frontend & Backend Programmer</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project team size:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">3</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">A system for planning team work</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Tools and technologies:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">HTML, JavaScript, jQuery, PHP, MySQL</div>
              </div>
            </div>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#8 <span className="text thin">Project name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">MONT EDI</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(October 2015 - December 2015, 3 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Customer:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">MONT</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project role:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Programmer (Server)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project team size:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">2</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Electronic Data Interchange</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Tools and technologies:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">C#, ASP.NET, BizTalk, Python</div>
              </div>
            </div>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#9 <span className="text thin">Project name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">ORIO Network</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(August 2015 - November 2015, 4 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Customer:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">ORIO</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project role:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Frontend & Backend Programmer</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project team size:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">7 Person</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Project description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Private Social Network</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Tools and technologies:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">HTML, Node JS, Meteor JS, JavaScript, Photoshop, Illustrator, Swift (IOS Development)</div>
              </div>
            </div>

          </div>
        </div>

        <div className="container align-center title margin-top-6"><span>I can think of</span></div>
        <div className="container align-center title small"><span>#Idea #Live #Thinkof</span></div>

        <div className="container white margin-top-6 padding-vertical-5">
          <div className="container center be align-left padding-horizontal-2">
            <h2 className="text upper thin align-center margin-0">What do you want from me?</h2>

            <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-cloud"></span> Official Experience</h3>

            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#0 <span className="text thin">Company name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">MONT</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(April 2014 - March 2016, 23 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Position:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Programmer</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Development of logistic systems</div>
              </div>
            </div>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#1 <span className="text thin">Company name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">BMSTU Experiments and Government Orders</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(February 2013 - February 2014, 12 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Position:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Laser Technology Engineer 2nd category</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Development of programs for research processes with the use of laser equipment</div>
              </div>
            </div>


            <div className="padding-horizontal-d1 margin-top-3">
              <div className="text">#2 <span className="text thin">Company name:</span></div>
              <div className="text thin sub margin-top-1 padding-horizontal-1">Dascon-Stroi</div>
            </div>

            <div className="padding-horizontal-1">
              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Involvement duration:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">(February 2009 - February 2010, 12 months)</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Position:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Web Programmer</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-2">
                <div className="text thin">Description:</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">Creation and support of site</div>
              </div>
            </div>

          </div>
        </div>

        <div className="container align-center title margin-top-6"><span>I believe in you</span></div>
        <div className="container align-center title small"><span>#Believe #YouCan #You</span></div>

        <div className="container align-center title small padding-bottom-2 margin-top-6"><span>makame#2016</span></div>
      </div>
    )
  }
})
