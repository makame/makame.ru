import React from 'react'
import ReactDOM from 'react-dom'
import Link from '../Controls/Link.jsx'
import $ from 'jquery';
import Velocity from 'velocity-animate';

export default React.createClass({

  render() {
    return (
      <div className="container align-center margin-top-5">
        <div className="container align-center title medium padding-bottom-3"><img className="avatar" src="/avataralt.jpg"></img></div>
        <div className="container align-center title medium padding-bottom-3"><span>Contacts</span></div>
        <div className="container align-center title"><span>I love work</span></div>
        <div className="container align-center title small"><span>#Motivation #Responsibility #Reason</span></div>

        <div className="inlines container be center align-center margin-top-2 text thin">
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://www.facebook.com/profile.php?id=100001115479981"><span className="fa fa-facebook"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="skype:makamekm?chat"><span className="fa fa-skype"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://twitter.com/makame_"><span className="fa fa-twitter"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://bitbucket.org/makame/"><span className="fa fa-bitbucket"></span></a>
          </div>
        </div>

        <div className="container bee center inlines strict align-center margin-top-4">
          <div className="boxed-container">
            <div className="boxed margin-horizontal-d1 margin-top-1">
              <h3 className="text thin upper margin-top-2 padding-horizontal-2">How can you get me?</h3>
              <div className="text thin sub margin-top-2 padding-horizontal-2">Skype? Email? Phone? Facebook?</div>
              <div>
                <div className="text thin sub margin-top-2 padding-vertical-1 padding-horizontal-2 row">My contact data are:</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">Skype</div><div className="cell padding-vertical-1 padding-horizontal-2"><a href="skype:makamekm?chat">makamekm</a></div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">Email</div><div className="cell padding-vertical-1 padding-horizontal-2"><a href="mailto:makame@mail.ru">makame@mail.ru</a></div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">Phone</div><div className="cell padding-vertical-1 padding-horizontal-2">+7 (968) 014 70 08</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">Facebook</div><div className="cell padding-vertical-1 padding-horizontal-2"><a href="https://www.facebook.com/profile.php?id=100001115479981">100001115479981</a></div></div>
              </div>
            </div>
          </div>
        </div>

        <div className="container align-center title margin-top-7"><span>I believe in you</span></div>
        <div className="container align-center title small"><span>#Believe #YouCan #You</span></div>

        <div className="container align-center title small margin-top-5 padding-bottom-2"><span>makame#2016</span></div>
      </div>
    )
  }
})
