import React from 'react'
import ReactDOM from 'react-dom'
import Link from '../Controls/Link.jsx'
import $ from 'jquery';
import Velocity from 'velocity-animate';

export default React.createClass({

  render() {
    return (
      <div className="container align-center margin-top-5">
        <div className="container align-center title medium padding-bottom-3"><span>Prices</span></div>
        <div className="container align-center title"><span>I love work</span></div>
        <div className="container align-center title small"><span>#Motivation #Responsibility #Reason</span></div>

        <div className="container bee center inlines pinned should-4 pc-2 tablet-1 mobile-1 align-center margin-top-4">
          <div className="boxed-container">
            <div className="boxed margin-horizontal-d1 margin-top-1">
              <span className="fa fa-stethoscope text gard-4 obtain margin-top-3" style={{fontSize: '7rem', lineHeight: '7rem'}}></span>
              <h3 className="text thin upper margin-top-2 padding-horizontal-2">Consultation</h3>
              <div className="text thin sub margin-top-2 padding-horizontal-2">I'll create any difficulty project for you</div>
              <div className="text thin sub margin-top-2 padding-vertical-1 padding-horizontal-2 row">I'll create any difficulty project for you</div>
              <div>
                <div className="text thin sub padding-vertical-1 padding-horizontal-2 row header">Talking</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">1 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">90$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">2 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">180$</div></div>
              </div>
              <div>
                <div className="text thin sub padding-vertical-1 padding-horizontal-2 row header">Chat</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">1 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">90$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">2 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">180$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">3 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">270$</div></div>
              </div>
              <div>
                <div className="text thin sub padding-vertical-1 padding-horizontal-2 row header">Chat</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">1 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">90$</div></div>
              </div>
            </div>
          </div>
          <div className="boxed-container">
            <div className="boxed margin-horizontal-d1 margin-top-1">
              <span className="fa fa-home text gard-2 obtain margin-top-3" style={{fontSize: '7rem', lineHeight: '7rem'}}></span>
              <h3 className="text thin upper margin-top-2 padding-horizontal-2">Frelance</h3>
              <div className="text thin sub margin-top-2 padding-horizontal-2">I'll create any difficulty project for you</div>
              <div className="text thin sub margin-top-2 padding-vertical-1 padding-horizontal-2 row">I'll create any difficulty project for you</div>
              <div>
                <div className="text thin sub padding-vertical-1 padding-horizontal-2 row header">Talking</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">1 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">90$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">2 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">180$</div></div>
              </div>
              <div>
                <div className="text thin sub padding-vertical-1 padding-horizontal-2 row header">Chat</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">1 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">90$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">2 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">180$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">3 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">270$</div></div>
              </div>
              <div>
                <div className="text thin sub padding-vertical-1 padding-horizontal-2 row header">Chat</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">1 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">90$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">2 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">180$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">3 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">270$</div></div>
              </div>
            </div>
          </div>
          <div className="boxed-container">
            <div className="boxed margin-horizontal-d1 margin-top-1">
              <span className="fa fa-laptop text gard-1 obtain margin-top-3" style={{fontSize: '7rem', lineHeight: '7rem'}}></span>
              <h3 className="text thin upper margin-top-2 padding-horizontal-2">FullTime Remote</h3>
              <div className="text thin sub margin-top-2 padding-horizontal-2">I'll create any difficulty project for you</div>
              <div className="text thin sub margin-top-2 padding-vertical-1 padding-horizontal-2 row">I'll create any difficulty project for you</div>
              <div>
                <div className="text thin sub padding-vertical-1 padding-horizontal-2 row header">Talking</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">1 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">90$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">2 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">180$</div></div>
              </div>
              <div>
                <div className="text thin sub padding-vertical-1 padding-horizontal-2 row header">Chat</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">1 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">90$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">2 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">180$</div></div>
              </div>
            </div>
          </div>
          <div className="boxed-container">
            <div className="boxed margin-horizontal-d1 margin-top-1">
              <span className="fa fa-building-o text gard-5 obtain margin-top-3" style={{fontSize: '7rem', lineHeight: '7rem'}}></span>
              <h3 className="text thin upper margin-top-2 padding-horizontal-2">FullTime</h3>
              <div className="text thin sub margin-top-2 padding-horizontal-2">I'll create any difficulty project for you</div>
              <div className="text thin sub margin-top-2 padding-vertical-1 padding-horizontal-2 row">I'll create any difficulty project for you</div>
              <div>
                <div className="text thin sub padding-vertical-1 padding-horizontal-2 row header">Talking</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">1 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">90$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">2 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">180$</div></div>
              </div>
              <div>
                <div className="text thin sub padding-vertical-1 padding-horizontal-2 row header">Chat</div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">1 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">90$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">2 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">180$</div></div>
                <div className="text sub row inlines strict"><div className="text thin cell padding-vertical-1 padding-horizontal-2">3 hour</div><div className="cell padding-vertical-1 padding-horizontal-2">270$</div></div>
              </div>
            </div>
          </div>
        </div>

        <div className="container align-center title margin-top-7"><span>I believe in you</span></div>
        <div className="container align-center title small"><span>#Believe #YouCan #You</span></div>

        <div className="container align-center title small margin-top-5 padding-bottom-2"><span>makame#2016</span></div>
      </div>
    )
  }
})
