import React from 'react'
import ReactDOM from 'react-dom'
import Link from '../Controls/Link.jsx'
import $ from 'jquery';
import Velocity from 'velocity-animate';

export default React.createClass({

  scrolled: false,

  showAll() {
    if (this.scrolled == false) {
      var contentscroll = ReactDOM.findDOMNode(this.refs.contentscroll);
      Velocity(contentscroll, { opacity: 1 }, { duration: 0 })
      Velocity(contentscroll, { opacity: 0 }, { display: "none", duration: 100, easing: "easeInSine" })

      var content = ReactDOM.findDOMNode(this.refs.content);
      Velocity(content, { opacity: 0 }, { duration: 0 })
      Velocity(content, { opacity: 1 }, { display: "", duration: 500, delay: 100, easing: "easeInSine" })

      this.scrolled = true
    }
  },

  hideAll() {
    const self = this

    if (this.scrolled == true) {

      var contentscroll = ReactDOM.findDOMNode(this.refs.contentscroll);
      Velocity(contentscroll, { opacity: 0 }, { duration: 0 })
      Velocity(contentscroll, { opacity: 1 }, { display: "", duration: 100, delay: 500, easing: "easeInSine",
        complete: function() {
          self.scrolled = false
        }
      })

      var content = ReactDOM.findDOMNode(this.refs.content);
      Velocity(content, { opacity: 1 }, { duration: 0 })
      Velocity(content, { opacity: 0 }, { display: "none", duration: 500, easing: "easeInSine",
        complete: function() {
          window.scrollTo(0, 0)
        }
      })
    }
  },

  componentDidMount() {
    const self = this
    $(window).scroll(function() {
      self.showAll()
    })
  },

  render() {
    return (
      <div className="container align-center margin-top-5">
        <div className="container align-center title medium padding-bottom-3"><img className="avatar" src="/avataralt.jpg"></img></div>
        <div className="container align-center title medium padding-bottom-3"><span>makame</span></div>
        <div className="container align-center title"><span>I will help you</span></div>
        <div className="container align-center title small"><span>#Design #Web #Desktop #Programmer</span></div>

        <div className="inlines container be center align-center margin-top-2 text thin">
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://www.facebook.com/profile.php?id=100001115479981"><span className="fa fa-facebook"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="skype:makamekm?chat"><span className="fa fa-skype"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://twitter.com/makame_"><span className="fa fa-twitter"></span></a>
          </div>
          <div className="padding-horizontal-d1">
            <a className="button transparent" href="https://bitbucket.org/makame/"><span className="fa fa-bitbucket"></span></a>
          </div>
        </div>

        <div ref="contentscroll" style={{opacity: 1, willChange: 'opacity'}}>
          <div className="container align-center button transparent margin-top-d4" onClick={this.showAll}><span><span className="fa fa-angle-down"></span> Scroll Down <span className="fa fa-angle-down"></span></span></div>
        </div>

        <div ref="content" style={{opacity: 0, display: 'none', willChange: 'opacity'}}>
          <div className="container white margin-top-7 padding-vertical-5">
            <div className="container center be align-left padding-horizontal-2">
              <h2 className="text upper thin align-center margin-0">What can I offer to you?</h2>

              <div className="inlines strict mobile-2 container center align-center margin-top-4">
                <div className="padding-horizontal-d1 margin-top-1">
                  <span className="fa fa-calendar text gard-1" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-1">
                  <span className="fa fa-group text gard-1" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-1">
                  <span className="fa fa-code text gard-1" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-1">
                  <span className="fa fa-comments text gard-1 shaddow-1" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
              </div>

              <div className="inlines container center align-center margin-top-5">
                <Link to={'/contacts'} className="button purple">Get Contact</Link>
              </div>
            </div>
          </div>


          <div className="inlines container be center align-center margin-top-6 text thin">
            <div className="padding-horizontal-d1">
              <a className="button transparent" href="https://en.wikipedia.org/wiki/Cascading_Style_Sheets#CSS_3"><span className="fa fa-css3"></span></a>
            </div>
            <div className="padding-horizontal-d1">
              <a className="button transparent" href="https://en.wikipedia.org/wiki/HTML5"><span className="fa fa-html5"></span></a>
            </div>
          </div>

          <div className="container align-center title margin-top-3"><span>I know many things</span></div>
          <div className="container align-center title small"><span>#System #Programming #Knowledge</span></div>

          <div className="inlines container be center align-center margin-top-2 text thin">
            <div className="padding-horizontal-d1">
              <a className="button transparent" href="https://en.wikipedia.org/wiki/Microsoft_Windows"><span className="fa fa-windows"></span></a>
            </div>
            <div className="padding-horizontal-d1">
              <a className="button transparent" href="https://en.wikipedia.org/wiki/Android_(operating_system)"><span className="fa fa-android"></span></a>
            </div>
            <div className="padding-horizontal-d1">
              <a className="button transparent" href="https://en.wikipedia.org/wiki/IOS"><span className="fa fa-apple"></span></a>
            </div>
            <div className="padding-horizontal-d1">
              <a className="button transparent" href="https://en.wikipedia.org/wiki/Linux"><span className="fa fa-linux"></span></a>
            </div>
          </div>

          <div className="container white margin-top-6 padding-vertical-5">
            <div className="container center be align-left padding-horizontal-2">
              <h2 className="text upper thin align-center margin-0">What technology do you want?</h2>

              <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-cloud"></span> BackEnd</h3>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> C/C++</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Server Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#c #c++</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> C#</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Business Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#csharp</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> Java</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Business Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#java</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> Python</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast and Flex Application</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#python</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> Go</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Server Applications but not widely spread</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#go</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> PHP</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast and Simple Server Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#php #apache #iis</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> Ruby</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast and Simple Server Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ruby</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> JavaScript via NodeJS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Flexible Server Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#javascript #node</div>
              </div>


              <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-code"></span> FrontEnd</h3>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> HTML5</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#html</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> CSS/SCSS/SASS/LESS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#css #scss #sass #less</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> JavaScript</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#javascript</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> ReactJS</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#javascript #framework</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> AngularJS (1 & 2)</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#javascript #framework</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> Swift (IOS & Android)</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #native</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> Java (Android)</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#andriod #native</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> React Core (IOS & Android)</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
              </div>


              <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-cogs"></span> FullStack Frameworks</h3>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> MeteorJS (JS)</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#javascript #framework</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> Django (Python)</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#python #framework</div>
              </div>

              <div className="padding-horizontal-d1 margin-top-3">
                <div className="text thin"><span className="fa fa-circle text nano"></span> Foundation (PHP)</div>
                <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#php #framework</div>
              </div>


              <div className="inlines container center align-center margin-top-5">
                <Link to={'/resume'} className="button blue">Get Resume</Link>
              </div>
            </div>
          </div>

          <div className="container align-center title margin-top-6"><span>I will explain you</span></div>
          <div className="container align-center title small"><span>#Work #Type #Info</span></div>

          <div className="container white margin-top-6 padding-vertical-5">
            <div className="container center be align-left padding-horizontal-2">
              <h2 className="text upper thin align-center margin-0">What kind of work do you want?</h2>

              <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-eye"></span> FrontEnd</h3>

              <div className="inlines strict mobile-1 container center align-center margin-top-4">
                <div className="padding-horizontal-d1 margin-top-2">
                  <span className="fa fa-tv text gard-2" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <h3 className="text thin upper margin-top-2">Desktop</h3>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-2">
                  <span className="fa fa-mobile text gard-2" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <h3 className="text thin upper margin-top-2">Mobile</h3>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
              </div>

              <div className="inlines strict mobile-1 container center align-center">
                <div className="padding-horizontal-d1 margin-top-2">
                  <span className="fa fa-html5 text gard-2" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <h3 className="text thin upper margin-top-2">Html/JavaScript</h3>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-2">
                  <span className="fa fa-gamepad text gard-2 shaddow-1" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <h3 className="text thin upper margin-top-2">Game</h3>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
              </div>


              <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-terminal"></span> BackEnd</h3>

              <div className="inlines strict mobile-1 container center align-center margin-top-4">
                <div className="padding-horizontal-d1 margin-top-2">
                  <span className="fa fa-server text gard-3" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <h3 className="text thin upper margin-top-2">WSDL Service</h3>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-2">
                  <span className="fa fa-cloud text gard-3" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <h3 className="text thin upper margin-top-2">REST/DDP</h3>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-2">
                  <span className="fa fa-database text gard-3" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <h3 className="text thin upper margin-top-2">Database</h3>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
              </div>


              <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-briefcase"></span> FullStack</h3>

              <div className="inlines strict mobile-1 container center align-center margin-top-4">
                <div className="padding-horizontal-d1 margin-top-2">
                  <span className="fa fa-clone text gard-4" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <h3 className="text thin upper margin-top-2">Framework</h3>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-2">
                  <span className="fa fa-cubes text gard-4" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <h3 className="text thin upper margin-top-2">HardCore</h3>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-2">
                  <span className="fa fa-ambulance text gard-4" style={{fontSize: '5rem', lineHeight: '5rem'}}></span>
                  <h3 className="text thin upper margin-top-2">All in One</h3>
                  <div className="text thin sub margin-top-2">I'll create any difficulty project for you</div>
                </div>
              </div>


              <div className="inlines container center align-center margin-top-5">
                <Link to={'/prices'} className="button red">Get Prices</Link>
              </div>
            </div>
          </div>

          <div className="container align-center title margin-top-6"><span>I can think of</span></div>
          <div className="container align-center title small"><span>#Idea #Live #Thinkof</span></div>

          <div className="container white margin-top-6 padding-vertical-5">
            <div className="container center be align-left padding-horizontal-2">
              <h2 className="text upper thin align-center margin-0">Do you know what you want?</h2>

              <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-globe"></span> Web Product</h3>

              <div className="inlines strict mobile-1 container center align-left">
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Single Page</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Web App</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
              </div>

              <div className="inlines strict mobile-1 container center align-left">
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Business Intranet App</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Service</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
              </div>

              <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-tv"></span> Desktop Product</h3>

              <div className="inlines strict mobile-1 container center align-left">
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Console App</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Desktop App</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
              </div>

              <div className="inlines strict mobile-1 container center align-left">
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Business Intranet App</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Game App</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
              </div>

              <h3 className="text upper thin align-left margin-0 margin-top-5"><span className="fa fa-mobile"></span> Mobile Product</h3>

              <div className="inlines strict mobile-1 container center align-left">
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Web App</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Presentation App</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
              </div>

              <div className="inlines strict mobile-1 container center align-left">
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Business App</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
                <div className="padding-horizontal-d1 margin-top-3">
                  <div className="text thin"><span className="fa fa-circle text nano"></span> Game App</div>
                  <div className="text thin sub margin-top-1 padding-horizontal-1">This is most used for Fast Applications</div>
                  <div className="text thin subsub mini margin-top-1 padding-horizontal-1">#ios #andriod #javascript</div>
                </div>
              </div>


              <div className="inlines container center align-center margin-top-5">
                <Link to={'/blog'} className="button green">Read Blog</Link>
              </div>
            </div>
          </div>

          <div className="container align-center title margin-top-6"><span>I believe in you</span></div>
          <div className="container align-center title small"><span>#Believe #YouCan #You</span></div>

          <div className="container align-center title small margin-top-6"><span>makame#2016</span></div>

          <div className="container align-center button transparent margin-top-d4 margin-bottom-3" onClick={this.hideAll}><span><span className="fa fa-angle-up"></span> Scroll To Top <span className="fa fa-angle-up"></span></span></div>
        </div>
      </div>
    )
  }
})
