import React from 'react'
// import ReactDOM from 'react-dom'
import { render } from 'react-dom'
// var ReactRouter = require('react-router')
import { Router, IndexRoute, IndexRedirect, Route, browserHistory } from 'react-router'
// const {Router, Route, IndexRoute} = ReactRouter
// var Route = ReactRouter
// var IndexRoute = ReactRouter.IndexRoute
// var Router = ReactRouter.Router
import $ from 'jquery';
import Velocity from 'velocity-animate';

var NotFoundPage = React.createClass({
  render() {
    return (
      <div className="container align-center padding-3">
        <h1 className="margin-bottom-d3">[404]</h1>
        <h2 className="margin-bottom-1">Page Not Found</h2>
        <div>You can try another route...</div>
      </div>
    )
  }
})

import HomeLayout from './Layouts/Home.jsx'
import HelloPage from './Pages/Hello.jsx'
import PricesPage from './Pages/Prices.jsx'
import ResumePage from './Pages/Resume.jsx'
import ContactsPage from './Pages/Contacts.jsx'
import BlogLayout from './Layouts/Blog.jsx'
import BlogPage from './Pages/Blog.jsx'
import BlogsPage from './Pages/Blogs.jsx'

import AdminLayout from './Layouts/AdminLayout.jsx'

import Link from './Controls/Link.jsx'
var TestPage = React.createClass({

  contextTypes: {
    pane: React.PropTypes.object,
    panes: React.PropTypes.object,
  },

  componentDidMount() {
    this.context.pane.setTitle('TEST 2')
    // this.context.pane.setWidth(1000)
  },

  render() {
    return (
      <div className="container align-center padding-3">
        <h1 className="margin-bottom-d3">[TEST]</h1>
        <h2 className="margin-bottom-1">Page is Test</h2>
        <div>You can try another route...</div>
        <Link to={'/admin/testtwo'} className="margin-top-3" activeClassName="active" onClick={this.hide}>Go</Link>
      </div>
    )
  }
})

var TestPageTwo = React.createClass({

  contextTypes: {
    pane: React.PropTypes.object,
    panes: React.PropTypes.object,
  },

  componentDidMount() {
    this.context.pane.setTitle('TEST 2')
    // this.context.pane.setWidth(1000)
  },

  closeDialog() {
    this.context.pane.removeDialog('test')
  },

  openDialog() {
    this.context.pane.scrollStart()
    this.context.pane.addDialog('test',
    (
      <div className="padding-2">
        <div className="margin-bottom-1 text sub">Tell me. Do you bleed?</div>
        <div className="link" onClick={this.closeDialog}>Yes</div>
        <div className="link" onClick={this.closeDialog}>No</div>
      </div>
    ))
  },

  render() {
    return (
      <div className="container align-center padding-3">
        <h1 className="margin-bottom-d3">[TEST 2]</h1>
        <h2 className="margin-bottom-1">Page is Test Two</h2>
        <div>You can try another route...</div>
        <Link to={'/admin/testthree'} className="margin-top-3" activeClassName="active" onClick={this.hide}>Go</Link>
        <div className="link" onClick={this.openDialog}>Open Dialog</div>
      </div>
    )
  }
})

var TestPageThree = React.createClass({

  contextTypes: {
    pane: React.PropTypes.object,
    panes: React.PropTypes.object,
  },

  componentDidMount() {
    this.context.pane.setTitle('TEST 2')
    // this.context.pane.setWidth(1000)
  },

  render() {
    return (
      <div className="container align-center padding-3">
        <h1 className="margin-bottom-d3">[TestPageThree]</h1>
        <h2 className="margin-bottom-1">Page is Test</h2>
        <div>You can try another route...</div>
        <Link to={'/admin/test'} className="margin-top-3" activeClassName="active" onClick={this.hide}>Go</Link>
      </div>
    )
  }
})

var routes = (
  <Router history={browserHistory}>
    <Route path="/admin" component={AdminLayout}>
      <IndexRedirect to="test"/>
      <Route path="test" component={TestPage}/>
      <Route path="testtwo" component={TestPageTwo}/>
      <Route path="testthree" component={TestPageThree}/>
      <Route path="*" component={NotFoundPage}/>
    </Route>
    <Route path="/" component={HomeLayout}>
      <IndexRedirect to="hello"/>
      <Route path="hello" component={HelloPage}/>
      <Route path="prices" component={PricesPage}/>
      <Route path="resume" component={ResumePage}/>
      <Route path="contacts" component={ContactsPage}/>
      <Route path="blog" component={BlogLayout}>
        <IndexRoute component={BlogsPage}/>
        <Route path=":blogId" component={BlogPage}/>
      </Route>
      <Route path="*" component={NotFoundPage}/>
    </Route>
  </Router>
)

render(routes, document.getElementById("app-container"))

$(window).load(function() {
  Velocity($('#loading').children(), 'fadeOut', {duration: 100, complete: function(){Velocity($('#loading'), 'fadeOut', {duration: 500});}});
});
