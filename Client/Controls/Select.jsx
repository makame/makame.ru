import React from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery';
import _ from 'underscore';
import ContentEditable from './ContentEditable.jsx'
import Popup from './Popup.jsx'
import Helper from '../Common/Helper.jsx'
import Compare from './Compare.jsx'

export default React.createClass({

  contextTypes: {
    popupService: React.PropTypes.object,
  },

  historyBefore: [],
  historyFuture: [],

  getInitialState() {
    return {
      items: [{text: "sel 3", id: 3, new: true, deletable: false}, {text: "sel 2", id: 2, new: false, deletable: true}],
      current: "",
      selected_tag: null,
      selected_row: null,
      guid: Helper.guid()
    }
  },

  getDefaultProps() {
    return {
      linkState: null,
      multiselectable: true,
      newable: true,
      max: null,
      disabled: false,
      className: '',
      placeholder: 'Select...',
      textEmpty: 'Nothing found',
      passiveItems: [{id: 1, text: "sel 1"}, {id: 2, text: "sel 2"}, {id: 3, text: "sel 3"}],
      search: null,
    }
  },

  standartSearch(text, callback) {
    var self = this
    setTimeout(function () {
      var values =  _.filter(self.props.passiveItems, function(s){ return s.text.search(text) >= 0 })
      callback(values)
    }, 150);
  },

  handleChange(value) {
    // console.log(this.refs.input.value());

    if ((this.historyBefore.length > 0 && JSON.stringify(this.historyBefore[this.historyBefore.length - 1].items) != JSON.stringify(this.state.items)) || this.historyBefore.length == 0)
    {
      if (this.historyBefore.length > 100) {
        this.historyBefore.shift()
      }
      this.historyBefore.push({items: $.extend(true, {}, this.state.items), current: this.state.current, pos: $.extend(true, {}, this.pos)});
      this.historyFuture = [];
    }

    var text = $('<div/>').append($.parseHTML(this.refs.input.value())).text().replace('&nbsp;', ' ')

    if (value != undefined) {
      text = value
    }

    this.setState({items: this.state.items, current: text})

    if (this.props.linkState) {
      this.props.linkState.requestChange(this.state.items)
    }
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.linkState) {
      this.setState({items: nextProps.linkState.value})
    }
  },

  componentDidMount() {
    if (this.props.linkState) {
      this.state.items = this.props.linkState.value
      this.setState({items: this.state.items})
    }
    this.startSearch()
  },

  onKeyDown(e) {
    this.pos = this.getSelectionCharOffsetsWithin(ReactDOM.findDOMNode(this.refs.input))

    if (e.ctrlKey || e.metaKey) {
      if (e.which == 90)
      {
        e.preventDefault()
        // alert('Undo');
        // console.log(this.historyBefore.pop())
        var hback = this.historyBefore.pop()
        if (hback) {
          if (this.historyFuture.length > 100) {
            this.historyFuture.shift()
          }
          this.historyFuture.push({items: $.extend(true, {}, this.state.items), current: this.state.current, pos: $.extend(true, {}, this.pos)})
          this.setState({items: hback.items, current: hback.current})
          this.pos = hback.pos
          this.select(this.pos)
        }
      }
      else if (e.which == 89)
      {
        e.preventDefault()
        var hfut = this.historyFuture.pop()
        if (hfut) {
          if (this.historyBefore.length > 100) {
            this.historyBefore.shift()
          }
          this.historyBefore.push({items: $.extend(true, {}, this.state.items), current: this.state.current, pos: $.extend(true, {}, this.pos)})
          this.setState({items: hfut.items, current: hfut.current})
          this.pos = hfut.pos
          this.select(this.pos)
        }
        // alert('Redo');
      }
    }
    else if (e.which == 8) {
      if (this.state.selected_tag != null) {
        e.preventDefault()
        if (this.state.items[this.state.selected_tag].deletable) {
          this.state.items.splice(this.state.selected_tag, 1);
          if (this.state.selected_tag > 0) {
            this.state.selected_tag--
          }
          else {
            this.state.selected_tag = null
          }
          this.setState({selected_tag: this.state.selected_tag})
          this.handleChange()
        }
      }
      else if (this.pos.start == this.pos.end && this.pos.start > 0 && this.pos.start != this.state.current.length) {
        this.pos.start = this.pos.start - 1;
        this.pos.end = this.pos.start;
      }
      else if (this.pos.start > 0) {
        this.pos.end = this.pos.start;
      }
    }
    else if (e.which == 13) {
      e.preventDefault()
      if (this.props.multiselectable && this.props.newable) {
        if (this.state.selected_row != null && this.state.selected_row < this.filteredValues().length) {
          var selected = this.filteredValues()[this.state.selected_row]
          var unexisting = _.where(this.state.items, {id: selected.id})
          if (unexisting.length == 0) {
            this.state.items.push({text: selected.text, id: selected.id, new: false, deletable: true})
            this.handleChange('')
          }
        }
        else if (this.state.current.trim().length > 1) {
          this.state.items.push({text: this.state.current, id: Helper.guid(), new: true, deletable: true})
          this.handleChange('')
        }
        $(ReactDOM.findDOMNode(this.refs.input)).blur()
        $(ReactDOM.findDOMNode(this.refs.input)).focus()
      }
      else if (this.props.multiselectable && !this.props.newable) {
        if (this.state.selected_row != null && this.state.selected_row < this.filteredValues().length) {
          var selected = this.filteredValues()[this.state.selected_row]
          this.state.items.push({text: selected.text, id: selected.id, new: false, deletable: true})
          this.handleChange('')
          $(ReactDOM.findDOMNode(this.refs.input)).blur()
          $(ReactDOM.findDOMNode(this.refs.input)).focus()
        }
      }
      else if (!this.props.multiselectable && this.props.newable) {
        if (this.state.selected_row != null && this.state.selected_row < this.filteredValues().length) {
          var selected = this.filteredValues()[this.state.selected_row]
          var unexisting = _.where(this.state.items, {id: selected.id})
          if (unexisting.length == 0) {
            this.state.items = [{text: selected.text, id: selected.id, new: false, deletable: true}]
            this.handleChange('')
          }
        }
        else if (this.state.current.trim().length > 1) {
          this.state.items = [{text: this.state.current, id: Helper.guid(), new: true, deletable: true}]
          this.handleChange('')
        }
        $(ReactDOM.findDOMNode(this.refs.input)).blur()
        $(ReactDOM.findDOMNode(this.refs.input)).focus()
      }
      else if (!this.props.multiselectable && !this.props.newable) {
        if (this.state.selected_row != null && this.state.selected_row < this.filteredValues().length) {
          var selected = this.filteredValues()[this.state.selected_row]
          this.state.items = [{text: selected.text, id: selected.id, new: false, deletable: true}]
          this.handleChange('')
          $(ReactDOM.findDOMNode(this.refs.input)).blur()
          $(ReactDOM.findDOMNode(this.refs.input)).focus()
        }
      }

      this.setState({selected_tag: null})
    }
    else if (e.which == 37) {
      e.preventDefault()
      if (this.state.selected_tag != null) {
        if (this.state.selected_tag > 0) {
          this.state.selected_tag--
        }
      }
      else if (this.pos.start == 0) {
        this.state.selected_tag = this.state.items.length - 1
      }
      else if (this.pos.start > 0) {
        if (this.pos.end == this.pos.start) {
          this.pos.start--
          this.pos.end--
        }
        else {
          this.pos.start--
        }
      }

      this.setState({selected_tag: this.state.selected_tag})

      this.select(this.pos)
    }
    else if (e.which == 39) {
      e.preventDefault()
      if (this.state.selected_tag != null) {
        if (this.state.selected_tag >= this.state.items.length - 1) {
          this.state.selected_tag = null
        }
        else {
          this.state.selected_tag++
        }
      }
      else if (this.pos.end < $(ReactDOM.findDOMNode(this.refs.input)).text().replace('&nbsp;', ' ').length) {
        if (this.pos.end == this.pos.start) {
          this.pos.start++
          this.pos.end++
        }
        else {
          this.pos.end++
        }
      }

      this.setState({selected_tag: this.state.selected_tag})

      this.select(this.pos)
    }
    else if (e.which == 38) {
      e.preventDefault()
      if (this.state.selected_row != null) {
        if (this.state.selected_row > 0) {
          this.state.selected_row--
        }
        else {
          this.state.selected_row = null
        }
      }

      this.setState({selected_row: this.state.selected_row})
    }
    else if (e.which == 40) {
      e.preventDefault()
      if (this.state.selected_row != null) {
        if (this.state.selected_row < this.filteredValues().length - 1) {
          this.state.selected_row++
        }
        else if (this.filteredValues().length > 0) {
          this.state.selected_row = this.filteredValues().length - 1
        }
        else {
          this.state.selected_row = null
        }
      }
      else {
        this.state.selected_row = 0
      }

      this.setState({selected_row: this.state.selected_row})
    }
    else {
      this.pos.start = this.pos.start + 1;
      this.pos.end = this.pos.start;
      this.setState({selected_tag: null})
    }
  },

  searchTimeout: null,
  loading: false,
  loadingIndex: 0,
  values: [],

  startSearch() {
    if (this.searchTimeout) {
      clearTimeout(this.searchTimeout)
    }

    this.loading = true
    this.loadingIndex++
    this.showPopup()

    const self = this

    this.searchTimeout = setTimeout(function(){
      var index = self.loadingIndex + 0

      const callback = function(values) {
        if (self.loadingIndex == index) {
          self.loading = false
          self.values = values
          self.state.selected_row = self.filteredValues().length > 0 ? 0 : null
          self.setState({selected_row: self.state.selected_row})
          self.showPopup()
        }
      }

      if (self.props.search == null) {
        self.standartSearch(self.state.current, callback)
      }
      else {
        self.props.search(self.state.current, callback)
      }
    }, 1);
  },

  onKeyUp(e) {
    if (e.which != 37 && e.which != 38 && e.which != 39 && e.which != 40) {
      this.startSearch()
    }
    this.onFocus()
  },

  onClick(e) {
    e.stopPropagation()
    e.preventDefault()

    // this.selection = this.saveSelection()
    this.onFocus()
  },

  waitClosePopup: false,

  onFocus() {
    this.focused = true
    this.pos = this.getSelectionCharOffsetsWithin(ReactDOM.findDOMNode(this.refs.input))
    this.waitClosePopup = false
    this.showPopup()
  },

  onBlur() {
    this.focused = false
    this.setState({selected_tag: null})
    this.hidePopup()
  },

  onWaitClosePopup() {
    this.waitClosePopup = true
  },

  showPopup() {
    if (this.props.disabled || !this.focused) {
      return
    }

    var values = _.map(this.filteredValues(), this.renderValue)

    var editorPanel = null
    var editorButtons = []
    var className = ["popup-menu", "max-height-10", "waited"]

    if (this.loading) {
      className.push("loading")
    }

    editorPanel = (
      <div className={className.join(' ')} key="panel">
        {values.length > 0 ? values : (<div className="header">No items</div>)}
      </div>
    )

    var element = $(ReactDOM.findDOMNode(this.refs.input))

    this.context.popupService.showPopup({html: editorPanel, guid: this.state.guid, offset: 9, element: element, position: "bottom center"})
  },

  hidePopup() {
    if (this.props.disabled || this.waitClosePopup || this.focused) {
      return
    }

    this.context.popupService.hidePopup(this.state.guid, null)
  },

  getSelectionCharOffsetsWithin(element) {
    var start = 0, end = 0;
    var range;

    try {
      range = window.getSelection().getRangeAt(0);
    } catch (e) {
      return {}
    }

    var index = 0

    _.each(ReactDOM.findDOMNode(this.refs.input).childNodes, function(item, indexItem) {
      var currentNode = item

      if (range.startContainer == currentNode) {
        start = index + range.startOffset
      }

      if (range.endContainer == currentNode) {
        end = index + range.endOffset
      }

      index += $(item).text().replace('&nbsp;', ' ').length
    })

    return {
      start: start,
      end: end
    };
  },

  select(pos) {
    var startNode, endNode, startOffset, endOffset
    var index = 0

    if (!pos.start) {
      pos.start = 0
    }
    if (!pos.end) {
      pos.end = 0
    }

    const length = $(ReactDOM.findDOMNode(this.refs.input).firstChild).text().replace('&nbsp;', ' ').length

    for (var i = 0; i < length; i++) {
      if (index == pos.start) {
        startOffset = i
        startNode = ReactDOM.findDOMNode(this.refs.input).firstChild
      }
      if (index == pos.end) {
        endOffset = i
        endNode = ReactDOM.findDOMNode(this.refs.input).firstChild
      }
      index++
    }

    if (!startNode) {
      if (ReactDOM.findDOMNode(this.refs.input).firstChild) {
        startOffset = length
        startNode = ReactDOM.findDOMNode(this.refs.input).firstChild
      }
      else {
        startOffset = length
        startNode = ReactDOM.findDOMNode(this.refs.input)
      }
    }

    if (!endNode) {
      if (ReactDOM.findDOMNode(this.refs.input).firstChild) {
        endOffset = length
        endNode = ReactDOM.findDOMNode(this.refs.input).firstChild
      }
      else {
        endOffset = length
        endNode = ReactDOM.findDOMNode(this.refs.input)
      }
    }

    var range = document.createRange();
    if (startNode) {
      range.setStart(startNode, startOffset);
    }
    if (endNode) {
      range.setEnd(endNode, endOffset);
    }
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  },

  removeItem(index) {
    this.state.items.splice(index, 1)
    this.setState({items: this.state.items})
  },

  renderItem(item, index) {
    var className = ['tag']
    var text = item.text

    if (this.state.selected_tag == index) {
      className.push('hover')
    }

    var remove = null

    if (item.deletable && !this.props.disabled) {
      remove = (<span className="remove fa fa-trash" onClick={this.removeItem.bind(this, index)}></span>)
    }

    return (
      <span
        className={className.join(' ')}
        key={item.id}>
        <span>{text}</span>{remove}
        </span>
      )
    },

    onClickValue(selected, e) {
      e.preventDefault()
      if (this.props.multiselectable) {
        this.state.items.push({text: selected.text, id: selected.id, new: false, deletable: true})
        this.handleChange('')
        $(ReactDOM.findDOMNode(this.refs.input)).blur()
        $(ReactDOM.findDOMNode(this.refs.input)).focus()
      }
      else {
        this.state.items = [{text: selected.text, id: selected.id, new: false, deletable: true}]
        this.handleChange('')
        $(ReactDOM.findDOMNode(this.refs.input)).blur()
        $(ReactDOM.findDOMNode(this.refs.input)).focus()
      }
    },

    renderValue(item, index) {
      var className = ['item']
      var text = item.text

      if (this.state.selected_row == index) {
        className.push('hover')
      }

      return (
        <div
          className={className.join(' ')}
          onMouseDown={this.onWaitClosePopup}
          onClick={this.onClickValue.bind(this, item)}
          key={item.id}>
          <span>{text}</span>
        </div>
      )
    },

    filteredValues() {
      const self = this
      return _.filter(this.values, function(s){
        return _.where(self.state.items,
          {
            id: s.id
          }
        ).length == 0
      })
    },

    value() {
      return this.state.items
    },

    componentDidUpdate() {
      if (!!this.pos && this.focused && !this.disabled) {
        this.select(this.pos)
      }
      else {
        this.hidePopup()
      }
    },

    render() {
      var tags = _.map(this.state.items, this.renderItem)

      return (
        <div className={"select " + this.props.className}>
          {tags}
          {
            this.props.disabled
            ?
            ""
            :
            (<ContentEditable
              onFocus={this.onFocus}
              onBlur={this.onBlur}
              className="input"
              ref="input"
              html={this.state.current.replace(' ', '&nbsp;')}
              placeholder={this.props.placeholder}
              disabled={this.props.disabled}
              onChange={this.handleChange.bind(this, undefined)}
              onKeyDown={this.onKeyDown}
              onKeyUp={this.onKeyUp}
              onClick={this.onClick}
              onTouch={this.onClick}/>)
            }
          </div>
        );
      }
    })
