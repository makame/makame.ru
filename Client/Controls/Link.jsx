import React from 'react'
import { Link, browserHistory } from 'react-router'

export default React.createClass({
  onClick(e) {
    if (this.props.onClick) {
      this.props.onClick(e)
    }
  },

  render() {
    const {onClick, children, ...props} = this.props
    return <Link {...props} onClick={this.onClick}>{children}</Link>
  }
})
