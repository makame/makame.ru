import _ from 'underscore';

// ChangeType is "None", "Add", "Remove", "Changed", "Conflict"

class ComparedBlock
{
  constructor()
  {
    this.Lines = []
    this.LinesOld = []
    this.LinesAlt = []
    this.Start = 0
    this.Type = "None"
  }
}

class Pair
{
  constructor()
  {
    this.mainPositionStart = -1
    this.mainPositionEnd = -1
    this.changePositionStart = -1
    this.changePositionEnd = -1
  }
}

export default class Comparer
{
  //Initial construction
  constructor(main, changed)
  {
    this.main = main;
    if (changed != null && changed != undefined) {
      this.changed = changed;
      //Recognized result
      this.lines = this.doCompare(main, changed);
    }
  }

  doCompare(changed)
  {
    if (this.changed == null)
    {
      this.lines = this.doCompare(this.main, changed);
      this.changed = changed;
    }
    else
    {
      this.lines = this.addCompare(this.main, this.lines, changed);
      this.changed = changed;
    }

    return this.lines;
  }
  //Addiction new Comparable File
  addCompare(main, before, addicted)
  {
    var result = [];

    //Comparing main & addicted file
    var newBlocks = this.doCompare(main, addicted);

    //Creation current block for filling
    var currentBlock = null;

    //Anon function for easy filling Current Block
    var addData = function(str, strOld, strAlt)
    {
      if (str != null && str != undefined)
      {
        currentBlock.Lines.push(str);
      }

      if (strOld != null && strOld != undefined)
      {
        currentBlock.LinesOld.push(strOld);
      }

      if (strAlt != null && strAlt != undefined)
      {
        currentBlock.LinesAlt.push(strAlt);
      }
    }

    //Anon function for easy filling Result
    var addLine = function(index, str, strOld, strAlt, type)
    {
      if (currentBlock == null)
      {
        currentBlock = new ComparedBlock()

        currentBlock.Start = index
        currentBlock.Type = type

        addData(str, strOld, strAlt);
      }
      else
      {
        if (currentBlock.Type == type) {
          addData(str, strOld, strAlt);
        }
        else
        {
          result.Add(currentBlock);

          currentBlock = new ComparedBlock()

          currentBlock.Start = index
          currentBlock.Type = type

          addData(str, strOld, strAlt);
        }
      }
    };


    //i - is line position at Main File
    var i = 0;

    _.each(before, function(blockMain) {
      _.each(blockMain.Type == "Add" ? [1] : blockMain.LinesOld, function() {
        //Get addicted blocks on current line
        var newBlock = _.filter(newBlocks, function(s) {
          return i == s.Start || (i >= s.Start && i < s.Start + s.LinesOld.length);
        })

        //Storing type for saving old Lines
        var type = "None";

        //Foreach addicted blocks on current line
        _.each(newBlock, function(block) {
          if (blockMain.Type == "None")
          {
            if (block.Type == "None")
            {
              addLine(i, block.Lines[i - block.Start], null, null, block.Type);
              type = block.Type;
            }
            else
            {
              if (block.Type != "Add")
              addLine(i, block.Lines[i - block.Start], null, null, block.Type);

              type = block.Type;

              _.each(_.rest(block.Lines, block.LinesOld.length), function(stringNew) {
                addLine(i, stringNew, null, null, block.Type);
              })
            }
          }
          else
          {
            if (block.Type == "None")
            {
              if (blockMain.Type != "Add")
              addLine(i, blockMain.Lines[i - blockMain.Start], null, null, blockMain.Type);

              type = blockMain.Type;

              _.each(_.rest(blockMain.Lines, blockMain.LinesOld.length), function(stringNew) {
                addLine(i, stringNew, null, null, blockMain.Type);
              })
            }
            else if (block.Lines[i - block.Start] == null && blockMain.Lines[i - blockMain.Start] == null)
            {}
            else
            {
              if (blockMain.Type != "Add" && block.Type != "Add")
              addLine(i, block.Lines[i - block.Start], null, blockMain.Lines[i - blockMain.Start], "Conflict");

              type = "Conflict";

              _.each(_.rest(blockMain.Lines, blockMain.LinesOld.length), function(stringNew) {
                addLine(i, null, null, stringAlt, "Conflict");
              })

              _.each(_.rest(block.Lines, block.LinesOld.length), function(stringNew) {
                addLine(i, stringNew, null, null, "Conflict");
              })
            }
          }
        })

        //Go to the next line
        if (i + 1 <= blockMain.Start + blockMain.LinesOld.length)
        {
          addLine(i, null, blockMain.LinesOld[i - blockMain.Start], null, type);
          i++;
        }
      })
    })

    //Finalize Current Block
    if (currentBlock != null)
    {
      result.push(currentBlock);
    }

    return result;
  }

  //Comparing Main File & Changed File
  doCompare(main, changed)
  {
    //Get matched Blocks
    var map = this.mapPair(main, changed, 0, main.length - 1, 0, changed.length - 1);

    var Lines = [];

    var firstBlock = map[0];

    //Recognize space before matched blocks
    if (firstBlock != undefined && firstBlock != null)
    {
      //Type Changed
      if (firstBlock.mainPositionStart > 0 && firstBlock.changePositionStart > 0)
      {
        var comparedChangeBlock = new ComparedBlock();

        for (var i = 0; i < firstBlock.changePositionStart; i++)
        {
          comparedChangeBlock.Lines.push(changed[i]);
        }

        for (var i = 0; i < firstBlock.mainPositionStart; i++)
        {
          comparedChangeBlock.LinesOld.push(main[i]);
        }

        comparedChangeBlock.Type = "Changed";

        comparedChangeBlock.Start = 0;

        Lines.push(comparedChangeBlock);
      }
      //Type Remove
      else if (firstBlock.mainPositionStart > 0)
      {
        var comparedRemoveBlock = new ComparedBlock();

        for (var i = 0; i < firstBlock.mainPositionStart; i++)
        {
          comparedRemoveBlock.LinesOld.push(main[i]);
        }

        comparedRemoveBlock.Type = "Remove";

        comparedRemoveBlock.Start = 0;

        Lines.push(comparedRemoveBlock);
      }
      //Type Add
      else if (firstBlock.changePositionStart > 0)
      {
        var comparedNewBlock = new ComparedBlock();

        for (var i = 0; i < firstBlock.changePositionStart; i++)
        {
          comparedNewBlock.Lines.push(changed[i]);
        }

        comparedNewBlock.Type = "Add";

        comparedNewBlock.Start = 0;

        Lines.push(comparedNewBlock);
      }
    }

    //Recognize space among matched blocks
    for (var k = 0; k < map.length; k++)
    {
      //Preparing
      var block = map[k];
      var nextBlock = null;

      if (k < map.length - 1)
      {
        nextBlock = map[k + 1];
      }

      //Type None
      var comparedBlock = new ComparedBlock();

      for (var i = block.changePositionStart; i <= block.changePositionEnd; i++)
      {
        comparedBlock.Lines.push(changed[i]);
      }

      for (var i = block.mainPositionStart; i <= block.mainPositionEnd; i++)
      {
        comparedBlock.LinesOld.push(main[i]);
      }

      comparedBlock.Type = "None";

      comparedBlock.Start = block.mainPositionStart;

      Lines.push(comparedBlock);

      //Recognize below Current Block
      if (nextBlock != null)
      {
        //Type Add
        if (block.mainPositionEnd + 1 >= nextBlock.mainPositionStart && block.changePositionEnd + 1 < nextBlock.changePositionStart)
        {
          var comparedNewBlock = new ComparedBlock();

          for (var i = block.changePositionEnd + 1; i < nextBlock.changePositionStart; i++)
          {
            comparedNewBlock.Lines.push(changed[i]);
          }

          comparedNewBlock.Type = "Add";

          comparedNewBlock.Start = block.mainPositionEnd + 1;

          Lines.push(comparedNewBlock);
        }
        //Type Remove
        else if (block.changePositionEnd + 1 >= nextBlock.changePositionStart && block.mainPositionEnd + 1 < nextBlock.mainPositionStart)
        {
          var comparedRemoveBlock = new ComparedBlock();

          for (var i = block.mainPositionEnd + 1; i < nextBlock.mainPositionStart; i++)
          {
            comparedRemoveBlock.LinesOld.push(main[i]);
          }

          comparedRemoveBlock.Type = "Remove";

          comparedRemoveBlock.Start = block.mainPositionEnd + 1;

          Lines.push(comparedRemoveBlock);
        }
        //Type Changed
        else if (block.mainPositionEnd + 1 < nextBlock.mainPositionStart)
        {
          var comparedChangeBlock = new ComparedBlock();

          for (var i = block.changePositionEnd + 1; i < nextBlock.changePositionStart; i++)
          {
            comparedChangeBlock.Lines.push(changed[i]);
          }

          for (var i = block.mainPositionEnd + 1; i < nextBlock.mainPositionStart; i++)
          {
            comparedChangeBlock.LinesOld.push(main[i]);
          }

          comparedChangeBlock.Type = "Changed";

          comparedChangeBlock.Start = block.mainPositionEnd + 1;

          Lines.push(comparedChangeBlock);
        }
      }
      //If next block is null means that we get last block and should recognize space at the end
      else
      {
        //Type Changed
        if (block.mainPositionEnd + 1 < main.length && block.changePositionEnd + 1 < changed.length)
        {
          var comparedChangeBlock = new ComparedBlock();

          for (var i = block.changePositionEnd + 1; i < changed.length; i++)
          {
            comparedChangeBlock.Lines.push(changed[i]);
          }

          for (var i = block.mainPositionEnd + 1; i < main.length; i++)
          {
            comparedChangeBlock.LinesOld.push(main[i]);
          }

          comparedChangeBlock.Type = "Changed";

          comparedChangeBlock.Start = block.mainPositionEnd + 1;

          Lines.push(comparedChangeBlock);
        }
        //Type Remove
        else if (block.mainPositionEnd + 1 < main.length)
        {
          var comparedRemoveBlock = new ComparedBlock();

          for (var i = block.mainPositionEnd + 1; i < main.length; i++)
          {
            comparedRemoveBlock.LinesOld.push(main[i]);
          }

          comparedRemoveBlock.Type = "Remove";

          comparedRemoveBlock.Start = block.mainPositionEnd + 1;

          Lines.push(comparedRemoveBlock);
        }
        //Type Add
        else if (block.changePositionEnd + 1 < changed.length)
        {
          var comparedNewBlock = new ComparedBlock();

          for (var i = block.changePositionEnd + 1; i < changed.length; i++)
          {
            comparedNewBlock.Lines.push(changed[i]);
          }

          comparedNewBlock.Type = "Add";

          comparedNewBlock.Start = block.mainPositionEnd + 1;

          Lines.push(comparedNewBlock);
        }
      }
    }

    //If did't find block compared
    if (map.length == 0)
    {
      //Type Remove
      if (main.length > 0)
      {
        var comparedRemoveBlock = new ComparedBlock();

        for (var i = 0; i < main.length; i++)
        {
          comparedRemoveBlock.LinesOld.push(main[i]);
        }

        comparedRemoveBlock.Type = "Remove";

        comparedRemoveBlock.Start = 0;

        Lines.push(comparedRemoveBlock);
      }
      //Type Add
      if (changed.length > 0)
      {
        var comparedNewBlock = new ComparedBlock();

        for (var i = 0; i < changed.length; i++)
        {
          comparedNewBlock.Lines.push(changed[i]);
        }

        comparedNewBlock.Type = "Add";

        comparedNewBlock.Start = 0;

        Lines.push(comparedNewBlock);
      }
    }

    return Lines;
  }

  //Recursive making a Map of Blocks
  mapPair(main, changed, startMain, endMain, startChanged, endChanged)
  {
    var pairList = [];

    var pair = null;

    var min = 0;

    //Searching Maximum Block at Current Zone
    while (true)
    {
      //Finding Syntax Block
      var maxPair = this.FindSyntaxBlock(main, changed, startMain, endMain, startChanged, endChanged, min + 1);

      //If it is a first time & syntax block did't find we shoud Find Standart Block
      if (pair == null && maxPair == null)
      {
        maxPair = this.FindBlock(main, changed, startMain, endMain, startChanged, endChanged, min + 1);
      }

      //if none found come out
      if (maxPair == null)
      {
        break;
      }
      //Else remember Pair & Minimum size
      else
      {
        pair = maxPair;
        min = maxPair.mainPositionEnd - maxPair.mainPositionStart + 1;
      }
    }

    if (pair != null)
    {
      //Above
      pairList = pairList.concat(this.mapPair(main, changed, startMain, pair.mainPositionStart - 1, startChanged, pair.changePositionStart - 1));
      //Save Current matching
      pairList.push(pair);
      //Below
      pairList = pairList.concat(this.mapPair(main, changed, pair.mainPositionEnd + 1, endMain, pair.changePositionEnd + 1, endChanged));
    }

    return pairList;
  }

  //Finding Block with Special Syntax
  FindSyntaxBlock(main, changed, mainStart, mainEnd, changedStart, changedEnd, min)
  {
    for (var i = mainStart; i < main.length && i <= mainEnd; i++)
    {
      for (var k = changedStart; k < changed.length && k <= changedEnd; k++)
      {
        for (var size = (mainEnd - i > changedEnd - k) ? changedEnd - k + 1 : mainEnd - i + 1; size >= min; size--)
        {
          if (this.CheckPair(main, changed, i, k, size) && main[i].trim().length >= 2 && main[i + size - 1].trim().length >= 2 && changed[k].trim().length >= 2 && changed[k + size - 1].trim().length >= 2)
          {
            var pair = new Pair()
            pair.mainPositionStart = i
            pair.mainPositionEnd = i + size - 1
            pair.changePositionStart = k
            pair.changePositionEnd = k + size - 1
            return pair
          }
        }
      }
    }

    return null;
  }

  //Finding Simple Block
  FindBlock(main, changed, mainStart, mainEnd, changedStart, changedEnd, min)
  {
    for (var i = mainStart; i < main.length && i <= mainEnd; i++)
    {
      for (var k = changedStart; k < changed.length && k <= changedEnd; k++)
      {
        for (var size = (mainEnd - i > changedEnd - k) ? changedEnd - k + 1 : mainEnd - i + 1; size >= min; size--)
        {
          if (this.CheckPair(main, changed, i, k, size))
          {
            var pair = new Pair()
            pair.mainPositionStart = i
            pair.mainPositionEnd = i + size - 1
            pair.changePositionStart = k
            pair.changePositionEnd = k + size - 1
            return pair
          }
        }
      }
    }

    return null;
  }

  //Checking if pair is equal
  CheckPair(main, changed, positionMainStart, positionChangedStart, count)
  {
    //Iterate Position Matching
    for (var i = 0; i < count; i++)
    {
      if (positionMainStart + i >= main.length || positionChangedStart + i >= changed.length)
      {
        return false;
      }
      else if (main[positionMainStart + i].trim() !== changed[positionChangedStart + i].trim())
      {
        return false;
      }
    }
    return true;
  }
}
