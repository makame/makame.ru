import React from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery';
import _ from 'underscore';
import ContentEditable from './ContentEditable.jsx'
import Popup from './Popup.jsx'
import Helper from '../Common/Helper.jsx'
import Compare from './Compare.jsx'

export default React.createClass({

  contextTypes: {
    popupService: React.PropTypes.object,
  },

  historyBefore: [],
  historyFuture: [],

  getInitialState() {
    return {
      items: [],
      guid: Helper.guid()
    }
  },

  getDefaultProps() {
    return {
      linkState: null,
      disabled: false,
      className: '',
      placeholder: 'Print...',
    }
  },

  getParentSelectedNode() {
    if (document.selection)
    return document.selection.createRange().parentElement();
    else
    {
      var selection = window.getSelection();
      if (selection.rangeCount > 0)
      return selection.getRangeAt(0).startContainer.parentNode;
    }
  },

  replaceSelection(html, selectInserted) {
    var sel, range, fragment;

    if (typeof window.getSelection != "undefined") {
      // IE 9 and other non-IE browsers
      sel = window.getSelection();

      // Test that the Selection object contains at least one Range
      if (sel.getRangeAt && sel.rangeCount) {
        // Get the first Range (only Firefox supports more than one)
        range = window.getSelection().getRangeAt(0);
        range.deleteContents();

        // Create a DocumentFragment to insert and populate it with HTML
        // Need to test for the existence of range.createContextualFragment
        // because it's non-standard and IE 9 does not support it
        if (range.createContextualFragment) {
          fragment = range.createContextualFragment(html);
        } else {
          // In IE 9 we need to use innerHTML of a temporary element
          var div = document.createElement("div"), child;
          div.innerHTML = html;
          fragment = document.createDocumentFragment();
          while ( (child = div.firstChild) ) {
            fragment.appendChild(child);
          }
        }
        var firstInsertedNode = fragment.firstChild;
        var lastInsertedNode = fragment.lastChild;
        range.insertNode(fragment);
        if (selectInserted) {
          if (firstInsertedNode) {
            range.setStartBefore(firstInsertedNode);
            range.setEndAfter(lastInsertedNode);
          }
          sel.removeAllRanges();
          sel.addRange(range);
        }
      }
    } else if (document.selection && document.selection.type != "Control") {
      // IE 8 and below
      range = document.selection.createRange();
      range.pasteHTML(html);
    }
  },

  handleChange() {
    // console.log(this.refs.input.value());

    if ((this.historyBefore.length > 0 && JSON.stringify(this.historyBefore[this.historyBefore.length - 1].items) != JSON.stringify(this.state.items)) || this.historyBefore.length == 0)
    {
      if (this.historyBefore.length > 100) {
        this.historyBefore.shift()
      }
      this.historyBefore.push({items: $.extend(true, {}, this.state.items), pos: $.extend(true, {}, this.pos)});
      this.historyFuture = [];
    }

    var items = this.calcHtml(this.refs.input.value())
    this.state.items = items
    this.setState(this.state.items)

    if (this.props.linkState) {
      this.props.linkState.requestChange(this.state.items)
    }
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.linkState) {
      this.setState({items: nextProps.linkState.value})
    }
  },

  componentDidMount() {
    if (this.props.linkState) {
      this.setState({items: this.props.linkState.value})
    }
    // var self = this
    // var ref = ReactDOM.findDOMNode(this.refs.input)
    // ref.addEventListener("paste", function(evt) {
    //   self.onPaste(evt)
    // }, false);
  },

  renderRow(row, index) {
    const self = this

    var classes = [];

    if (row.attrs && row.attrs.margin_top && Number(row.attrs.margin_top)) {
      if (Number(row.attrs.margin_top) % 2 > 0) {
        classes.push("margin-top-d" + (Math.floor(Number(row.attrs.margin_top) / 2) + 1))
      }
      else {
        classes.push("margin-top-" + (Number(row.attrs.margin_top) / 2))
      }
    }

    if (row.attrs && row.attrs.padding_horizontal && Number(row.attrs.padding_horizontal)) {
      if (Number(row.attrs.padding_horizontal) % 2 > 0) {
        classes.push("padding-horizontal-d" + (Math.floor(Number(row.attrs.padding_horizontal) / 2) + 1))
      }
      else {
        classes.push("padding-horizontal-" + (Number(row.attrs.padding_horizontal) / 2))
      }
    }

    if (row.attrs && row.attrs.align) {
      if (row.attrs.align == "left") {
        classes.push("align-left")
      }
      else if (row.attrs.align == "right") {
        classes.push("align-right")
      }
      else if (row.attrs.align == "center") {
        classes.push("align-center")
      }
    }

    return '<div class="' + classes.join(' ') + '">' + _.map(row.items, function(item) {
      return self.renderItem(item)
    }).join("") + '</div>'
  },

  renderItem(item) {
    var classes = [];

    if (item.attrs) {

      if (!!item.attrs.src) {
        return '<img src="' + item.attrs.src + '">'
      }

      if (!!item.attrs.href) {
        if (item.text.trim() == '') {
          return ''
        }
        return '<a href="' + item.attrs.href + '">' + item.text + '</a>'
      }

      if (item.attrs.bold == 'true' || item.attrs.bold == true) {
        classes.push("bold")
      }
      if (item.attrs.underline == 'true' || item.attrs.underline == true) {
        classes.push("underline")
      }
      if (item.attrs.overline == 'true' || item.attrs.overline == true) {
        classes.push("overline")
      }
      if (item.attrs.header == 'true' || item.attrs.header == true) {
        classes.push("header")
      }
      if (item.attrs.italic == 'true' || item.attrs.italic == true) {
        classes.push("italic")
      }
      if (item.attrs.bigheader == 'true' || item.attrs.bigheader == true) {
        classes.push("bigheader")
      }
      if (item.attrs.mini == 'true' || item.attrs.mini == true) {
        classes.push("mini")
      }
      if (item.attrs.code == 'true' || item.attrs.code == true) {
        classes.push("code")
      }
      if (item.attrs.sub == 'true' || item.attrs.sub == true) {
        classes.push("sub")
      }
    }

    var text = item.text.replace(' ', '&nbsp;')

    if (classes.length == 0) {
      return text
    }

    return '<span class="' + classes.join(' ') + '">' + text + '</span>'
  },

  calcHtml(value, setSelection) {
    var self = this

    var result = [];

    $('<div/>').append($.parseHTML(value)).contents().each(function(index, itemRow) {
      var jitemRow = $(itemRow)
      var tagRow = jitemRow.prop("tagName")

      var bold = jitemRow.hasClass('bold')
      var underline = jitemRow.hasClass('underline')
      var overline = jitemRow.hasClass('overline')
      var header = jitemRow.hasClass('header')
      var italic = jitemRow.hasClass('italic')
      var code = jitemRow.hasClass('code')
      var bigheader = jitemRow.hasClass('bigheader')
      var mini = jitemRow.hasClass('mini')
      var sub = jitemRow.hasClass('sub')
      var align = null
      if (jitemRow.hasClass('align-left')) {
        align = "left"
      }
      else if (jitemRow.hasClass('align-right')) {
        align = "right"
      }
      else if (jitemRow.hasClass('align-center')) {
        align = "center"
      }
      var href = jitemRow.attr('href')
      var selection = setSelection

      var margin_top = 0
      var padding_horizontal = 0

      if (jitemRow.attr('class')) {
        jitemRow.attr('class').replace(/(margin-top-d[0-9])/g, function(str) {
          margin_top = Number(str.replace(/(margin-top-d)/g, '')) * 2 - 1
        });
        jitemRow.attr('class').replace(/(margin-top-[0-9])/g, function(str) {
          margin_top = Number(str.replace(/(margin-top-)/g, '')) * 2
        });
        jitemRow.attr('class').replace(/(padding-horizontal-d[0-9])/g, function(str) {
          padding_horizontal = Number(str.replace(/(padding-horizontal-d)/g, '')) * 2 - 1
        });
        jitemRow.attr('class').replace(/(padding-horizontal-[0-9])/g, function(str) {
          padding_horizontal = Number(str.replace(/(padding-horizontal-)/g, '')) * 2
        });
      }

      if (tagRow == 'DIV' || tagRow == 'SPAN' || tagRow == 'A') {
        result.push({
          items: self.getContent(jitemRow,
            {
              bold, underline, overline, header, italic, code, bigheader, mini, sub, href, selection
            },
            setSelection
          ),
          attrs: {
            align: align,
            margin_top: margin_top,
            padding_horizontal: padding_horizontal,
          }
        })
      }
      else if (tagRow == 'IMG') {
        var src = jitemRow.attr('src')
        result.push({
          items: [
            {
              text: "",
              attrs: {
                src
              }
            }
          ],
          attrs: {}
        })
      }
      else {
        result.push({
          items: [
            {
              text: $('<div/>').append(jitemRow.clone()).html().replace('&nbsp;', ' '),
              attrs: {
                bold, underline, overline, header, italic, code, bigheader, mini, sub, selection
              }
            }
          ],
          attrs: {}
        })
      }
    })

    // console.log(result);

    result = this.optimizeData(result)

    return result
  },

  getContent(jitemRow, attrs, setSelection) {
    const self = this

    var result = [];

    jitemRow.contents().each(function(index, item) {
      var jitem = $(item)
      var tag = jitem.prop("tagName")

      var bold = jitem.hasClass('bold') ? true : attrs.bold
      var underline = jitem.hasClass('underline') ? true : attrs.underline
      var overline = jitem.hasClass('overline') ? true : attrs.overline
      var header = jitem.hasClass('header') ? true : attrs.header
      var italic = jitem.hasClass('italic') ? true : attrs.italic
      var code = jitem.hasClass('code') ? true : attrs.code
      var bigheader = jitem.hasClass('bigheader') ? true : attrs.bigheader
      var mini = jitem.hasClass('mini') ? true : attrs.mini
      var sub = jitem.hasClass('sub') ? true : attrs.sub
      var href = !!attrs.href ? attrs.href : jitem.attr('href')
      var selection = setSelection

      if (tag == 'DIV' || tag == 'SPAN' || tag == 'A' || tag == 'FONT') {
        $.merge(result, self.getContent(jitem,
          {
            bold, underline, overline, header, italic, code, bigheader, mini, sub, href, selection
          },
          setSelection
        ))
      }
      else if (tag == 'IMG') {
        var src = jitem.attr('src')
        result.push({
          text: "",
          attrs: {
            src
          }
        })
      }
      else {
        result.push({
          text: $('<div/>').append(jitem.clone()).html().replace('&nbsp;', ' '),
          attrs: {
            bold, underline, overline, header, italic, code, bigheader, mini, sub, href, selection
          }
        })
      }
    })

    return result
  },

  optimizeData(data) {
    const self = this

    return _.map(data, function(row) {
      var optimRow = []
      var optimItem = null
      for (var i = 0; i < row.items.length; i++) {
        if (optimItem != null && self.compareItemAttr(row.items[i], optimItem)) {
          optimItem = {
            text: optimItem.text + row.items[i].text,
            attrs: optimItem.attrs,
          }
        }
        else if (optimItem == null) {
          optimItem = row.items[i]
        }
        else {
          optimRow.push(optimItem)
          optimItem = row.items[i]
        }

        if (row.items.length - 1 == i) {
          optimRow.push(optimItem)
        }
      }
      return {
        items: optimRow,
        attrs: row.attrs
      }
    })
  },

  compareItemAttr(left, right) {
    return left.attrs.bold == right.attrs.bold &&
    left.attrs.underline == right.attrs.underline &&
    left.attrs.overline == right.attrs.overline &&
    left.attrs.header == right.attrs.header &&
    left.attrs.italic == right.attrs.italic &&
    left.attrs.code == right.attrs.code &&
    left.attrs.bigheader == right.attrs.bigheader &&
    left.attrs.mini == right.attrs.mini &&
    left.attrs.sub == right.attrs.sub &&
    left.attrs.href == right.attrs.href &&
    left.attrs.src == right.attrs.src
  },

  selectElementContents(el, collapse) {
    var range = document.createRange();
    range.selectNodeContents(el);
    if (collapse) {
      range.collapse(false);
    }
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  },

  // onPaste(e) {
  //   return false;
  // },

  onKeyDown(e) {
    this.pos = this.getSelectionCharOffsetsWithin(ReactDOM.findDOMNode(this.refs.input))

    if (e.ctrlKey || e.metaKey) {
      if (e.which == 90)
      {
        e.preventDefault()
        // alert('Undo');
        // console.log(this.historyBefore.pop())
        var hback = this.historyBefore.pop()
        if (hback) {
          if (this.historyFuture.length > 100) {
            this.historyFuture.shift()
          }
          this.historyFuture.push({items: $.extend(true, {}, this.state.items), pos: $.extend(true, {}, this.pos)})
          this.setState({items: hback.items})
          this.pos = hback.pos
          this.select(this.pos)
        }
      }
      else if (e.which == 89)
      {
        e.preventDefault()
        var hfut = this.historyFuture.pop()
        if (hfut) {
          if (this.historyBefore.length > 100) {
            this.historyBefore.shift()
          }
          this.historyBefore.push({items: $.extend(true, {}, this.state.items), pos: $.extend(true, {}, this.pos)})
          this.pos = hfut.pos
          this.setState({items: hfut.items})
        }
        // alert('Redo');
      }
    }
    else if (e.which == 8) {
      if (this.pos.start == this.pos.end) {
        this.pos.start = this.pos.start - 1;
        this.pos.end = this.pos.start;
      }
      else {
        this.pos.start = this.pos.start;
        this.pos.end = this.pos.start;
      }
    }
    else {
      this.pos.start = this.pos.start + 1;
      this.pos.end = this.pos.start;
    }

    if ($(this.getParentSelectedNode()).parent().parent()[0] != ReactDOM.findDOMNode(this.refs.input) && $(this.getParentSelectedNode()).parent()[0] != ReactDOM.findDOMNode(this.refs.input)) {
      var contents = $(ReactDOM.findDOMNode(this.refs.input)).contents()
      var sel = window.getSelection()
      var pos = sel.anchorOffset - 1
      if (sel.anchorOffset == 0) {
        pos = 0
      }
      if (this.state.items.length == 1 && this.state.items[0].length == 1) {
        // console.log(contents);
        this.selectElementContents(contents[pos], true)
      }
    }
  },

  onKeyUp(e) {
    this.onFocus()

    if (e.ctrlKey || e.metaKey) {
    }
    else {
      if (e.which == 13) {
        if (this.props.onEnter) {
          this.props.onEnter(this)
        }
      }
      else if (e.which == 8) {
        if (this.props.onBack) {
          this.props.onBack(this)
        }
      }
      else if (e.which == 37) {
        if (this.props.onLeft) {
          this.props.onLeft(this)
        }
      }
      else if (e.which == 39) {
        if (this.props.onRight) {
          this.props.onRight(this)
        }
      }
      else {
      }
    }
  },

  onClick(e) {
    e.stopPropagation()
    e.preventDefault()

    // this.selection = this.saveSelection()
    this.onFocus()
  },

  onMouseUp() {
    // this.selection = this.saveSelection()
    this.onFocus()
  },

  getSelectionCharOffsetsWithin(element) {
    var start, end;
    var startRow, endRow;
    var sel, range, priorRange;

    try {
      range = window.getSelection().getRangeAt(0);
    } catch (e) {
      return {}
    }

    var index = 0

    _.each(ReactDOM.findDOMNode(this.refs.input).childNodes, function(row, rowIndex) {
      _.each(row.childNodes, function(item) {
        var currentNode = item
        if ($(item).prop("tagName") == "SPAN") {
          currentNode = item.childNodes[0]
        }
        else if ($(item).prop("tagName") == "BR") {
          currentNode = item.parentNode
        }

        if (range.startContainer == currentNode) {
          start = index + range.startOffset
          startRow = rowIndex
        }

        if (range.endContainer == currentNode) {
          end = index + range.endOffset
          endRow = rowIndex
        }

        for (var i = 0; i < $(item).text().replace('&nbsp;', ' ').length; i++) {
          index++
        }
      })
      index++
    })

    // if (typeof window.getSelection != "undefined") {
    //   range = window.getSelection().getRangeAt(0);
    //   priorRange = range.cloneRange();
    //   priorRange.selectNodeContents(element);
    //   priorRange.setEnd(range.startContainer, range.startOffset);
    //   start = priorRange.toString().length;
    //   end = start + range.toString().length;
    // }
    // else if (typeof document.selection != "undefined" && (sel = document.selection).type != "Control") {
    //   range = sel.createRange();
    //   priorRange = document.body.createTextRange();
    //   priorRange.moveToElementText(element);
    //   priorRange.setEndPoint("EndToStart", range);
    //   start = priorRange.text.length;
    //   end = start + range.text.length;
    // }

    return {
      start: start,
      end: end,
      startRow: startRow,
      endRow: endRow,
    };
  },

  select(pos) {
    var startNode, endNode, startOffset, endOffset
    var index = 0

    if (pos.start == undefined || pos.end == undefined) {
      return
    }

    _.each(ReactDOM.findDOMNode(this.refs.input).childNodes, function(row) {
      _.each(row.childNodes, function(item) {
        for (var i = 0; i < $(item).text().replace('&nbsp;', ' ').length; i++) {
          if (index == pos.start) {
            startOffset = i
            startNode = item
          }
          if (index == pos.end) {
            endOffset = i
            endNode = item
          }
          index++
        }
      })

      if (index == pos.start) {
        if (row.childNodes.length > 0) {
          startOffset = $(row.childNodes[row.childNodes.length - 1]).text().replace('&nbsp;', ' ').length
          startNode = row.childNodes[row.childNodes.length - 1]
        }
        else {
          startOffset = 0
          startNode = row.firstChild
        }
      }
      if (index == pos.end) {
        if (row.childNodes.length > 0) {
          endOffset = $(row.childNodes[row.childNodes.length - 1]).text().replace('&nbsp;', ' ').length
          endNode = row.childNodes[row.childNodes.length - 1]
        }
        else {
          endOffset = 0
          endNode = row.firstChild
        }
      }
      index++
    })

    var range = document.createRange();
    if (startNode) {
      if ($(startNode).prop("tagName") && $(startNode).prop("tagName") != "BR") {
        range.setStart(startNode.childNodes[0], startOffset);
      }
      else {
        range.setStart(startNode, startOffset);
      }
    }
    if (endNode) {
      if ($(endNode).prop("tagName") && $(endNode).prop("tagName") != "BR") {
        range.setEnd(endNode.childNodes[0], endOffset);
      }
      else {
        range.setEnd(endNode, endOffset);
      }
    }
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  },

  setParagraphAttr(proc) {
    // this.restoreSelection(this.selection)

    //this.pos = this.getSelectionCharOffsetsWithin(ReactDOM.findDOMNode(this.refs.input))

    for (var i = this.pos.startRow; i <= this.pos.endRow; i++) {
      this.state.items[i].attrs = proc(this.state.items[i].attrs, this.state.items[i], i)
    }

    this.setState({items: this.state.items})

    if (this.props.linkState) {
      this.props.linkState.requestChange(this.state.items)
    }

    this.select(this.pos)

    // this.hidePopup()

    // this.select({start: indexStart, end: indexStart + length})
  },

  setAttrs(proc) {
    var self = this

    // this.restoreSelection(this.selection)

    //this.pos = this.getSelectionCharOffsetsWithin(ReactDOM.findDOMNode(this.refs.input))

    var indexStart = 0

    var items = []

    _.each(this.state.items, function(row, index) {
      var cells = []

      _.each(row.items, function(item) {
        if (item.text == '<br>') {
          cells.push(item)
        }
        else {
          if ((self.pos.start >= indexStart && self.pos.start < indexStart + item.text.length) || (self.pos.end >= indexStart && self.pos.end < indexStart + item.text.length)) {
            var left = {
              text: item.text.substring(0, self.pos.start - indexStart),
              attrs: item.attrs,
            }
            if (left.text.length > 0) {
              cells.push(left)
            }

            var new_attr = $.extend(true, {}, item.attrs)
            new_attr = proc(new_attr, item.text.substring(self.pos.start - indexStart, self.pos.end - indexStart))
            var center = {
              text: item.text.substring(self.pos.start - indexStart, self.pos.end - indexStart),
              attrs: new_attr,
            }
            if (center.text.length > 0) {
              cells.push(center)
            }

            var right = {
              text: item.text.substring(self.pos.end - indexStart, item.text.length),
              attrs: item.attrs,
            }
            if (right.text.length > 0) {
              cells.push(right)
            }
          }
          else {
            cells.push(item)
          }

          indexStart += item.text.length
        }
      })

      indexStart += 1

      items.push({
        items: cells,
        attrs: row.attrs
      })
    })

    this.setState({items: items})

    if (this.props.linkState) {
      this.props.linkState.requestChange(items)
    }

    this.select(this.pos)

    // this.hidePopup()
  },

  setMarginTop() {
    this.setParagraphAttr(function(attrs) {
      if (Number(attrs.margin_top) >= 0) {
        attrs.margin_top = Number(attrs.margin_top) + 1
      }
      else {
        attrs.margin_top = 1
      }
      return attrs
    })
  },

  desetMarginTop() {
    this.setParagraphAttr(function(attrs) {
      if (Number(attrs.margin_top) >= 0) {
        attrs.margin_top = Number(attrs.margin_top) - 1
      }
      else {
        attrs.margin_top = 0
      }
      return attrs
    })
  },

  setPaddingHorizontal() {
    this.setParagraphAttr(function(attrs) {
      if (Number(attrs.padding_horizontal) >= 0) {
        attrs.padding_horizontal = Number(attrs.padding_horizontal) + 1
      }
      else {
        attrs.padding_horizontal = 1
      }
      return attrs
    })
  },

  desetPaddingHorizontal() {
    this.setParagraphAttr(function(attrs) {
      if (Number(attrs.padding_horizontal) >= 0) {
        attrs.padding_horizontal = Number(attrs.padding_horizontal) - 1
      }
      else {
        attrs.padding_horizontal = 0
      }
      return attrs
    })
  },

  setAlignLeft() {
    this.setParagraphAttr(function(attrs) {
      if (attrs.align == "left") {
        attrs.align = null
      }
      else {
        attrs.align = "left"
      }
      return attrs
    })
  },

  setAlignCenter() {
    this.setParagraphAttr(function(attrs) {
      if (attrs.align == "center") {
        attrs.align = null
      }
      else {
        attrs.align = "center"
      }
      return attrs
    })
  },

  setAlignRight() {
    this.setParagraphAttr(function(attrs) {
      if (attrs.align == "right") {
        attrs.align = null
      }
      else {
        attrs.align = "right"
      }
      return attrs
    })
  },

  setBold() {
    this.setAttrs(function(attrs, text){
      attrs.bold = !attrs.bold
      return attrs
    })
  },

  setItalic() {
    this.setAttrs(function(attrs, text){
      attrs.italic = !attrs.italic
      return attrs
    })
  },

  setHeader() {
    this.setAttrs(function(attrs, text){
      attrs.header = !attrs.header
      return attrs
    })
  },

  setCode() {
    this.setAttrs(function(attrs, text){
      attrs.code = !attrs.code
      return attrs
    })
  },

  setUnderline() {
    this.setAttrs(function(attrs, text){
      attrs.underline = !attrs.underline
      return attrs
    })
  },

  setOverline() {
    this.setAttrs(function(attrs, text){
      attrs.overline = !attrs.overline
      return attrs
    })
  },

  setBigHeader() {
    this.setAttrs(function(attrs, text){
      attrs.bigheader = !attrs.bigheader
      return attrs
    })
  },

  setMini() {
    this.setAttrs(function(attrs, text){
      attrs.mini = !attrs.mini
      return attrs
    })
  },

  setSub() {
    this.setAttrs(function(attrs, text){
      attrs.sub = !attrs.sub
      return attrs
    })
  },

  desetAll() {
    this.setAttrs(function(attrs, text){
      attrs.header = false
      attrs.code = false
      attrs.bold = false
      attrs.italic = false
      attrs.overline = false
      attrs.underline = false
      attrs.bigheader = false
      attrs.mini = false
      attrs.sub = false
      attrs.href = null
      return attrs
    })
  },

  setLink() {
    var url = window.prompt("Enter URL", "");

    if (url && url.length > 0) {
      this.setAttrs(function(attrs, text){
        attrs.header = false
        attrs.code = false
        attrs.bold = false
        attrs.italic = false
        attrs.overline = false
        attrs.underline = false
        attrs.bigheader = false
        attrs.mini = false
        attrs.sub = false
        attrs.href = url
        return attrs
      })
    }
  },

  addImage() {
    var url = window.prompt("Enter Image URL", "");

    // if (url && url.length > 0) {
    //   this.setAttrs(function(attrs, text){
    //     attrs.header = false
    //     attrs.code = false
    //     attrs.bold = false
    //     attrs.italic = false
    //     attrs.overline = false
    //     attrs.underline = false
    //     attrs.bigheader = false
    //     attrs.mini = false
    //     attrs.sub = false
    //     attrs.href = null
    //     attrs.src = url
    //     return attrs
    //   })
    // }

    var self = this

    // this.restoreSelection(this.selection)

    this.select(this.pos)

    this.replaceSelection('<img src="' + url + '">')

    this.select(this.pos)

    this.handleChange()
  },

  waitClosePopup: false,

  onFocus() {
    this.focused = true
    // this.setState({focused: true})
    // this.selection = this.saveSelection()
    this.pos = this.getSelectionCharOffsetsWithin(ReactDOM.findDOMNode(this.refs.input))
    this.waitClosePopup = false
    this.showPopup()
  },

  onBlur() {
    this.focused = false
    // this.setState({focused: false})
    this.hidePopup()
  },

  onWaitClosePopup() {
    this.waitClosePopup = true
  },

  showPopup(e) {
    if (this.props.disabled || !this.focused) {
      return
    }

    var editorPanel = null

    var editorButtons = []

    if (!this.props.disabled) {
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setMarginTop} key="margin1"><span className="fa fa-angle-double-down"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.desetMarginTop} key="demargin1"><span className="fa fa-angle-double-up"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setPaddingHorizontal} key="padding1"><span className="fa fa-indent"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.desetPaddingHorizontal} key="depadding1"><span className="fa fa-dedent"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setMini} key="mini"><span className="fa fa-compress"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setCode} key="code"><span className="fa fa-code"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setBold} key="bold"><span className="fa fa-bold"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setItalic} key="italic"><span className="fa fa-italic"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setSub} key="sub"><span className="fa fa-paragraph"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setUnderline} key="underline"><span className="fa fa-underline"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setOverline} key="overline"><span className="fa fa-strikethrough"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setHeader} key="header"><span className="fa fa-header"></span></div>
      )

      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setAlignLeft} key="left"><span className="fa fa-align-left"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setAlignCenter} key="center"><span className="fa fa-align-center"></span></div>
      )
      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setAlignRight} key="right"><span className="fa fa-align-right"></span></div>
      )

      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.desetAll} key="desetall"><span className="fa fa-eraser"></span></div>
      )

      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.setLink} key="link"><span className="fa fa-link"></span></div>
      )

      editorButtons.push(
        <div className="button mini transparent margin-vertical-d1" onMouseDown={this.onWaitClosePopup} onClick={this.addImage} key="image"><span className="fa fa-image"></span></div>
      )

      editorPanel = (
        <div className="inlines container center align-center padding-d1" key="panel">
          {editorButtons}
        </div>
      )
    }

    var element = $(ReactDOM.findDOMNode(this))

    this.context.popupService.showPopup({html: editorPanel, guid: this.state.guid, className: 'large', offset: 9, element: element, position: "top center"})
  },

  hidePopup(e) {
    if (this.props.disabled || this.waitClosePopup || this.focused) {
      return
    }

    this.context.popupService.hidePopup(this.state.guid, null)
  },

  value() {
    return this.state.items
  },

  componentDidUpdate() {
    const self = this

    if (this.props.reference !== undefined && this.state.items !== undefined && this.state.items.length > 0) {
      setTimeout(function(){
        var resultCompare

        const main = _.map(self.props.reference, function(row){
          return _.map(row.items, function(item) {
            return item.text
          }).join()
        })

        const changed = _.map(self.state.items, function(row){
          return _.map(row.items, function(item) {
            return item.text
          }).join()
        })

        resultCompare = new Compare(main, changed).lines

        if (resultCompare) {
          _.each($(ReactDOM.findDOMNode(self.refs.input)).children(), function(child, index) {
            var iter = 0
            _.find(resultCompare, function(itemRow, i) {
              var finded = _.find(itemRow.Lines, function(itemItem, k) {
                if (iter == index) {
                  if (itemRow.Type == "Add") {
                    $(child).css("background-color", "rgba(0, 255, 10, 0.075)")
                  }
                  else if (itemRow.Type == "Changed") {
                    $(child).css("background-color", "rgba(0, 110, 255, 0.075)")
                  }
                  return true
                }
                iter++
                return false
              })
              return !!finded
            })
          })
        }
      }, 5);
    }

    if (!!this.pos && this.focused && !this.disabled) {
      this.select(this.pos)
    }
    else {
      this.hidePopup()
    }
  },

  render() {
    var self = this
    var html = ""

    if (this.state.items.length == 0 || (this.state.items.length == 1 && this.state.items[0].length == 0) || (this.state.items.length == 1 && this.state.items[0].length == 1 && this.state.items[0][0].text.trim() == '')) {
      html = ""
    }
    else {
      html = _.map(this.state.items, function(row, index) {
        return self.renderRow(row, index)
      }).join("")
    }

    // console.log(html);

    return (
      <div>
        <ContentEditable
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          placeholder={this.props.placeholder}
          className={"editor " + this.props.className}
          ref="input" html={html}
          disabled={this.props.disabled}
          onChange={this.handleChange.bind(this)}
          onKeyDown={this.onKeyDown}
          onKeyUp={this.onKeyUp}
          onMouseUp={this.onMouseUp}
          onTouchEnd={this.onMouseUp}
          onClick={this.onClick}
          onTouch={this.onClick}/>
      </div>
    );
  }
})
