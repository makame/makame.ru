import React from 'react'
import { PaneContainer, Pane } from '../Controls/PaneContainer.jsx'
import _ from 'underscore'
import ReactDOM from 'react-dom'
import Velocity from 'velocity-animate'
import Helper from '../Common/Helper.jsx'
import $ from 'jquery'

export default React.createClass({

  childContextTypes: {
    panes: React.PropTypes.object
  },

  getChildContext() {
    const self = this
    return {
      panes: {
        closeAll: function() {
          self.closeAll()
        },
        addPane: function(id, element) {
          if (id == null || id == undefined) {
            id = Helper.guid()
          }

          var find = _.find(self.state.items, { id: id })

          if (find) {
            throw new Exeption("such id is existing: " + id)
          }
          else {
            self.state.items.push({
              id: id,
              children: element,
            })
          }
        },
        scrollTop: function(value) {
          self.props.scrollTop(value)
        },
        scrollLeft: function(value) {
          self.props.scrollLeft(value)
        },
        scrollContainer: function() {
          return self.props.scrollContainer()
        },
        scrollPositionLeft: function() {
          return $(self.props.scrollContainer()).scrollLeft()
        }
      }
    }
  },

  getInitialState: function() {
    return {
      items: [],
      previousPathname: null,
    }
  },

  componentWillReceiveProps(nextProps, nextContext) {
    var find = _.find(this.state.items, { id: nextProps.location.pathname })

    if (!find) {
      this.state.items.push({
        id: nextProps.location.pathname + '',
        children: React.cloneElement(nextProps.children, {
          children: React.Children.only(nextProps.children)
        }),
      })

      this.setState({ previousPathname: this.props.location.pathname, items: this.state.items })
    }
  },

  componentDidMount() {
    const self = this
    this.state.items.push({
      id: this.props.location.pathname + '',
      children: React.cloneElement(self.props.children, {
        children: React.Children.only(self.props.children)
      }),
    })
    this.setState({ previousPathname: this.props.location.pathname, items: this.state.items })
  },

  componentDidUpdate() {
    if (this.state.previousPathname) {
      this.setState({ previousPathname: null })
    }
  },

  closeItem(item, index) {
    var newItems = []
    for (var i = 0; i < this.state.items.length; i++) {
      if (i != index) {
        newItems.push(this.state.items[i])
      }
    }
    this.state.items = newItems

    this.setState({ items: this.state.items })
  },

  closeAll() {
    this.state.items = []
    this.setState({ items: this.state.items })
  },

  renderItem(item, index) {
    return (
      <Pane
        id={item.id}
        key={item.id}
        eventClose={this.closeItem.bind(this, item, index)}>
        {item.children}
      </Pane>
    )
  },

  render() {
    var children = _.map(this.state.items, this.renderItem);

    return (
      <div className="relative margin-2">
        <PaneContainer
          className=""
          direction="horizontal"
          margin={15}
          location={this.props.location}>
          {children}
        </PaneContainer>
      </div>
    )
  }
})
