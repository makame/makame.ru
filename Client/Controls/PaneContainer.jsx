import React from 'react'
import ReactDOM from 'react-dom'
import _ from 'underscore'
import { Motion, spring } from 'react-motion'
import isEqual from 'lodash.isequal'
import PaneContent from './PaneContent.jsx'
import Pane from './Pane.jsx'
import $ from 'jquery'
import Anim from '../Controls/Anim.jsx'
import Velocity from 'velocity-animate'
import ReactTransitionGroup from 'react-addons-transition-group';

const reinsert = (array, from, to) => {
  const a = array.slice(0);
  const v = a[from];
  a.splice(from, 1);
  a.splice(to, 0, v);
  return a;
};

const clamp = (n, min = n, max = n) => Math.max(Math.min(n, max), min);

const springConfig = [500, 30];

var PaneContainer = React.createClass({

  contextTypes: {
    panes: React.PropTypes.object,
  },

  propTypes: {
    direction: React.PropTypes.oneOf(['horizontal', 'vertical']),
    margin: React.PropTypes.number,
    style: React.PropTypes.object,
    children: React.PropTypes.array,
    onResizeStart: React.PropTypes.func,
    onResize: React.PropTypes.func,
    onResizeStop: React.PropTypes.func,
    disableEffect: React.PropTypes.bool,
    onOrderChange: React.PropTypes.func,
    className: React.PropTypes.string,
    isResizable: React.PropTypes.shape({
      x: React.PropTypes.bool,
      y: React.PropTypes.bool,
      xy: React.PropTypes.bool,
    }),
  },

  getDefaultProps() {
    return {
      direction: 'horizontal',
      margin: 0,
      onClick: () => null,
      onTouchStart: () => null,
      onResizeStart: () => null,
      onResize: () => null,
      onResizeStop: () => null,
      onOrderChange: () => null,
      customStyle: {},
      className: '',
      disableEffect: false,
      isResizable: {
        x: true,
        y: false,
        xy: false,
      },
    }
  },

  getInitialState: function() {
    return {
      delta: 0,
      mouse: 0,
      isPressed: false,
      lastPressed: 0,
      isResizing: false,
      panes: this.props.children.map((child, order) => ({
        id: child.props.id,
        width: child.props.width,
        height: child.props.height,
        order,
      })),
    }
  },

  componentWillMount() {
    this.hasRemove = false;
    this.hasAdded = false;

    window.addEventListener('touchmove', this.handleTouchMove);
    window.addEventListener('touchend', this.handleMouseUp);
    window.addEventListener('mousemove', this.handleMouseMove);
    window.addEventListener('mouseup', this.handleMouseUp);
  },

  componentDidMount() {
    this.setSize();
  },

  componentWillUpdate(next) {
    const { panes } = this.state;
    return this.addPane(next);
    // return null;
  },

  componentDidUpdate() {
    if (this.hasAdded) {
      this.hasAdded = false;
      this.setSize();
    }
  },

  componentWillUnmount() {
    window.removeEventListener('touchmove', this.handleTouchMove);
    window.removeEventListener('touchend', this.handleMouseUp);
    window.removeEventListener('mousemove', this.handleMouseMove);
    window.removeEventListener('mouseup', this.handleMouseUp);
  },

  onResize(i, dir, size, rect) {
    let { panes } = this.state;
    const order = this.state.panes.map(pane => pane.order);
    panes = panes.map((pane, index) => {
      if (order.indexOf(i) === index) {
        return {
          width: rect.width,
          height: rect.height,
          order: pane.order,
          id: pane.id,
        };
      }
      return pane;
    });
    this.setState({ panes });
    this.props.onResize({ id: panes[order.indexOf(i)].id, dir, size, rect });
  },

  getPaneSizeList() {
    const width = this.state.panes.map(pane => pane.width);
    const height = this.state.panes.map(pane => pane.height);
    return this.isHorizontal() ? width : height;
  },

  getItemCountByPosition(position) {
    const size = this.getPaneSizeList();
    const { margin } = this.props;
    let sum = 0;
    if (position < 0) return 0;
    for (let i = 0; i < size.length; i++) {
      sum += size[i] + margin;
      if (sum >= position) return i + 1;
    }
    return size.length;
  },

  setSize() {
    const panes = []

    this.state.panes.forEach((pane, i) => {
      var { width, height } = ReactDOM.findDOMNode(this.refs.panes).children[pane.order].getBoundingClientRect()
      panes.push(Object.assign({}, pane, {width, height}))
    });

    if (!isEqual(panes, this.state.panes)) this.setState({ panes });
  },

  getItemPositionByIndex(index) {
    const size = this.getPaneSizeList();
    let sum = 0;
    for (let i = 0; i < index; i++) sum += size[i] + this.props.margin;
    return sum;
  },

  isHorizontal() {
    return this.props.direction === 'horizontal';
  },

  updateOrder(panes, index, mode) {
    return panes.map(pane => {
      if (pane.order >= index) {
        const { id, width, height, order } = pane;
        return { id, width, height, order: mode === 'add' ? order + 1 : order - 1 };
      }
      return pane;
    });
  },

  getVisible() {
    var min = 0;

    this.state.panes.forEach((pane, i) => {
      if (Math.abs(this.getItemPositionByIndex(i) - this.context.panes.scrollPositionLeft()) < Math.abs(this.getItemPositionByIndex(min) - this.context.panes.scrollPositionLeft())) {
        min = i
      }
    })

    return min;
  },

  addPane(next) {
    var min = this.getVisible();

    const newPanes = []
    const idsRemove = next.children.map(child => child.props.id);

    this.state.panes.forEach((pane) => {
      var order = idsRemove.indexOf(pane.id)
      if (order !== -1) {
        newPanes.push(Object.assign({}, pane, {order: order}))
      }
      else {
        this.hasRemove = true;
      }
    });

    next.children.forEach((child, i) => {
      const idsAdd = newPanes.map(pane => pane.id);
      var pos = idsAdd.indexOf(child.props.id)
      if (pos === -1) {
        const { id, width, height } = child.props;
        const pane = { id, width, height, order: i };
        newPanes.splice(min, 0, pane);
        this.hasAdded = true;
      }
    });

    if (!isEqual(newPanes, this.state.panes))
    {
      this.state.panes = newPanes
      this.setState({ panes: this.state.panes });
    }
  },

  handleResizeStart(i) {
    const order = this.state.panes.map(pane => pane.order);
    this.setState({ isResizing: true });
    this.props.onResizeStart({ id: this.state.panes[order.indexOf(i)].id });
  },

  handleResizeStop(i, dir, size, rect) {
    const { panes } = this.state;
    const order = this.state.panes.map(pane => pane.order);
    this.setState({ isResizing: false });
    this.props.onResizeStop({ id: panes[order.indexOf(i)].id, dir, size, rect });
  },

  handleMouseDown(pos, pressX, pressY, { pageX, pageY }) {
    this.setState({
      delta: this.isHorizontal() ? pageX - pressX : pageY - pressY,
      mouse: this.isHorizontal() ? pressX : pressY,
      isPressed: true,
      lastPressed: pos,
    });
  },

  handleMouseMove({ pageX, pageY }) {
    const { isPressed, delta, lastPressed, isResizing, panes } = this.state;
    const { onOrderChange } = this.props;
    if (isPressed && !isResizing) {
      const mouse = this.isHorizontal() ? pageX - delta : pageY - delta;
      const { length } = this.props.children;
      const order = this.state.panes.map(pane => pane.order);
      const row = clamp(Math.round(this.getItemCountByPosition(mouse)), 0, length - 1);
      const newPanes = reinsert(panes, order.indexOf(lastPressed), row);
      this.setState({ mouse, panes: newPanes });
      if (!isEqual(panes, newPanes))
      {
        onOrderChange(panes, newPanes);
      }
    }
  },

  handleTouchStart(pos, pressX, pressY, e) {
    this.handleMouseDown(pos, pressX, pressY, e.touches[0]);
  },

  handleTouchMove(e) {
    if (this.state.isPressed || this.state.isResizing) {
      e.preventDefault();
      this.handleMouseMove(e.touches[0]);
    }
  },

  handleMouseUp() {
    this.setState({ isPressed: false, delta: 0 });
  },

  renderPanes() {
    const { mouse, isPressed, lastPressed } = this.state;
    const order = this.state.panes.map(pane => pane.order);
    const { children, disableEffect } = this.props;
    return children.map((child, i) => {
      const springPosition = spring(this.getItemPositionByIndex(order.indexOf(i)), springConfig);
      const style = lastPressed === i && isPressed
      ? {
        scale: disableEffect ? 1 : spring(1.02, springConfig),
        shadow: disableEffect ? 0 : spring(16, springConfig),
        x: this.isHorizontal() ? mouse : 0,
        y: !this.isHorizontal() ? mouse : 0,
      }
      : {
        scale: spring(1, springConfig),
        shadow: spring(0, springConfig),
        x: this.isHorizontal() ? springPosition : 0,
        y: !this.isHorizontal() ? springPosition : 0,
      };

      return (
        <Motion style={style} key={child.props.id}>
          {({ scale, shadow, x, y }) => {
            const onResize = this.onResize.bind(this, i);
            const onMouseDown = this.handleMouseDown.bind(this, i, x, y);
            const onTouchStart = this.handleTouchStart.bind(this, i, x, y);
            const onResizeStart = this.handleResizeStart.bind(this, i);
            const onResizeStop = this.handleResizeStop.bind(this, i);
            return (
              <PaneContent
                onResize={onResize}
                key={child.props.id}
                style={Object.assign({}, child.props.style, {
                  boxShadow: `rgba(0, 0, 0, 0.2) 0px ${shadow}px ${2 * shadow}px 0px`,
                  zIndex: i === lastPressed ? 99 : i,
                  position: 'absolute',
                  left: `${x}px`,
                  top: `${y}px`,
                })}
                onResizeStart={onResizeStart}
                onResizeStop={onResizeStop}
                onMouseDown={onMouseDown}
                onTouchStart={onTouchStart}
                onClickClose={child.props.eventClose}>
                {child}
              </PaneContent>
            );
          }}
        </Motion>
      );
    });
  },

  render() {
    const self = this
    const { style, className } = this.props;
    return (
      <Anim
        component="div"
        ref="panes"
        className={"panes " + className}
        style={style}
        willEnter={
          function(el, callback){
            el.css('z-index', -1);
            // el.hide()
            // Velocity(el, { opacity: 0.0, scale: 1.0 }, { display: "none", duration: 0 })
            Velocity(el, { opacity: [1.0, 0.0], scale: [1.0, 0.9], zIndex:'' }, { duration: 300, easing: "swing", complete: callback })
          }
        }
        willAppear={
          function(el, callback){
            el.css('z-index', -1);
            // el.hide()
            // Velocity(el, { opacity: 0.0, scale: 1.0 }, { display: "none", duration: 0 })
            Velocity(el, { opacity: [1.0, 0.0], scale: [1.0, 0.9], zIndex:'' }, { duration: 300, easing: "swing", complete: callback })
          }
        }
        willLeave={
          function(el, callback){
            el.css('z-index', -1);
            Velocity(el, { opacity: 0.0, scale: 0.9 }, { duration: 300, easing: "swing", complete: callback })
          }
        }
        didEnter={
          function(el){
            self.forceUpdate()
          }
        }
        didAppear={
          function(el){
            self.forceUpdate()
          }
        }
        didLeave={
          function(el){
            self.forceUpdate()
          }
        }>
        {this.renderPanes()}
      </Anim>
    );
  }
})

export { Pane, PaneContainer };
