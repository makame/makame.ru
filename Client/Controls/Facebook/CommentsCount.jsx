import React from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery';
import Config from '../../../Config/Public_Config.js';

export default React.createClass({

  componentDidMount() {
    const self = this

    if (typeof FB !== "undefined") {
      const node = $(ReactDOM.findDOMNode(this))
      node.addClass('loading')
      FB.XFBML.parse($(ReactDOM.findDOMNode(this))[0], function() {
        node.removeClass('loading')
      })
    }
  },

  componentDidUpdate:function(){
    if (typeof FB !== "undefined") {
      const node = $(ReactDOM.findDOMNode(this))
      node.addClass('loading')
      FB.XFBML.parse($(ReactDOM.findDOMNode(this))[0], function() {
        node.removeClass('loading')
      })
    }
  },

  shouldComponentUpdate(nextProps) {
    return nextProps.href != this.props.href;
  },

  render() {
    return (
      <span className="waited">
        <span className="fb-comments-count" data-href={this.props.href}></span>
      </span>
    )
  }
})
