import React from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery';
import Config from '../../../Config/Public_Config.js';

export default React.createClass({

  componentDidMount() {
    const self = this

    if (typeof FB !== "undefined") {
      const node = $(ReactDOM.findDOMNode(this))
      node.addClass('loading')
      FB.XFBML.parse($(ReactDOM.findDOMNode(this))[0], function() {
        node.removeClass('loading')
      })
    }
  },

  componentDidUpdate:function(){
    if (typeof FB !== "undefined") {
      const node = $(ReactDOM.findDOMNode(this))
      node.addClass('loading')
      FB.XFBML.parse($(ReactDOM.findDOMNode(this))[0], function() {
        node.removeClass('loading')
      })
    }
  },

  shouldComponentUpdate(nextProps) {
    return nextProps.href != this.props.href;
  },

  render() {
    return (
      <div className="waited">
        <div className="fb-comments" data-href={this.props.href} data-width="100%" data-numposts="5" data-order-by="reverse_time" data-app-id={Config.facebook_api_key}></div>
      </div>
    )
  }
})
