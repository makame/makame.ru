import React, { Component, PropTypes } from 'react';

export default React.createClass({
  propTypes: {
    id: PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ]).isRequired,
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    minWidth: PropTypes.number,
    maxWidth: PropTypes.number,
    minHeight: PropTypes.number,
    maxHeight: PropTypes.number,
    style: PropTypes.object,
    className: PropTypes.string,
    children: PropTypes.any,
  },

  getDefaultProps() {
    return {
      style: {},
      className: '',
    }
  },

  render() {
    return (
      <div className={this.props.className}>{this.props.children}</div>
    );
  },
})
