import React from 'react'
import ReactDOM from 'react-dom'
import Helper from '../Common/Helper.jsx'
import ReactTransitionGroup from 'react-addons-transition-group';
import $ from 'jquery';
import Velocity from 'velocity-animate';
import _ from 'underscore'
const Snap = require('snapsvg');
import { browserHistory } from 'react-router'

const RouteChangerChild = React.createClass({
  shouldComponentUpdate(nextProps) {
    return !!nextProps.shouldUpdate;
  },

  cp: null,
  content: null,
  animation: false,
  animation_transition: false,

  getInitialState: function(){
    var self = this

    return {
      set_loading: function() {
        self.animation = true
        self.content.hide()
        Velocity(self.content, { opacity: 1.0 }, { display: "block", duration: self.props.animateDuration / 2 })
        self.cp.animate({ r: Math.sqrt($(window).width() * $(window).width() + $(window).height() * $(window).height()) / 2 }, self.props.animateDuration / 3)
      },

      unset_loading: function() {
        self.animation = false
        if (!self.animation_transition) {
          Velocity(self.content, { opacity: 0 }, { display: "none", duration: self.props.animateDuration / 3 , complete: function() {
            self.cp.animate({ r: 0 }, self.props.animateDuration / 3)
          }})
        }
      },
    }
  },

  childContextTypes: {
    set_loading: React.PropTypes.func,
    unset_loading: React.PropTypes.func,
  },

  getChildContext: function() {
    return {
      set_loading: this.state.set_loading,
      unset_loading: this.state.unset_loading,
    }
  },

  getDefaultProps() {
    return {
      id: Helper.guid()
    }
  },

  componentWillAppear(callback) {
    this._animateIn(callback)
  },

  componentDidAppear() {
  },

  componentWillEnter(callback) {
    const self = this
    self.animation_transition = true

    if (!self.animation) {
      self.content.hide()
      Velocity(self.content, { opacity: 1.0 }, { display: "block", duration: self.props.animateDuration / 2, complete: callback })
      self.cp.animate({ r: Math.sqrt($(window).width() * $(window).width() + $(window).height() * $(window).height()) / 2 }, self.props.animateDuration / 3)
    }

    setTimeout(function(){
      self.animation_transition = false
      if (!self.animation) {
        self.cp.animate({ r: 0 }, self.props.animateDuration / 3)
        Velocity(self.content, { opacity: 0 }, { display: "none", duration: self.props.animateDuration / 3, complete: function() {
          self.cp.animate({ r: 0 }, self.props.animateDuration / 3)
          callback()
        } })
      }
    }, self.props.animateDuration / 3 * 2);

    self._animateInDelay(callback)
  },

  componentWillMount() {
    const loader = Snap($('.pageload-overlay').find('svg')[0]);
    this.cp = loader.circle($(window).width() / 2, $(window).height() / 2, 0).attr({ fill: "rgb(9, 0, 13)", stroke: "none" });
    this.content = $('.pageload-overlay').find('.content')
  },

  componentDidMount() {
  },

  componentDidEnter() {
  },

  componentWillLeave(callback) {
    this._animateOut(callback)
  },

  componentDidLeave() {
  },

  _animateInDelay(callback) {
    var self = this
    var el = ReactDOM.findDOMNode(this);
    $(el).hide()
    Velocity($(el), { opacity: 1.0 }, { display: "block", duration: self.props.animateDuration / 2, delay: self.props.animateDuration / 2, complete: callback })
  },

  _animateIn(callback) {
    var self = this
    var el = ReactDOM.findDOMNode(this);
    $(el).hide()
    Velocity($(el), { opacity: 1.0 }, { display: "block", duration: self.props.animateDuration / 2, complete: callback })
  },

  _animateOut(callback) {
    var self = this
    var el = ReactDOM.findDOMNode(this);
    Velocity($(el), { opacity: 0 }, { display: "none", duration: self.props.animateDuration / 2, complete: callback })
  },

  render() {
    var child = this.props.children;
    if (child === null || child === false) {
      return null;
    }

    const {id, key, shouldUpdate, animateDuration, ...props} = this.props

    return React.cloneElement(this.props.children, {
      ...props,
      children: React.Children.only(this.props.children)
    });

    // return React.Children.only(this.props.children)
  }
})

export default React.createClass({
  // childContextTypes: {
  //   router: React.PropTypes.object
  // },
  //
  // getChildContext: function() {
  //   return {
  //     location: null
  //   };
  // },

  // contextTypes: {
  //   location: React.PropTypes.object,
  // },
  //
  // getContext: function() {
  //   return {
  //     location: null
  //   };
  // },

  getInitialState() {
    return {
      previousPathname: null
    }
  },

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.setState({ previousPathname: this.props.location.pathname })
    }
  },

  componentDidUpdate() {
    if (this.state.previousPathname) {
      this.setState({ previousPathname: null })
    }
  },

  render() {
    const { previousPathname } = this.state
    const { duration, ...props } = this.props

    var childCount = React.Children.count(this.props.children);
    var children = React.Children.map(this.props.children, function(child, idx) {
      return (
        <RouteChangerChild
          key={previousPathname || this.props.location.pathname}
          shouldUpdate={!previousPathname}
          animateDuration={this.props.duration}
          loader={this.refs.loader}
          content={this.refs.content}
          {...props}>
          {child}
        </RouteChangerChild>
      )
    }.bind(this));

    return (
      <ReactTransitionGroup
        transitionLeave={true}>
        {children}
      </ReactTransitionGroup>
    )
  }
})
