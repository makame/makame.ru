import React from 'react'
import ReactDOM from 'react-dom'
import Resizable from 'react-resizable-box'
import Anim from '../Controls/Anim.jsx'
import Velocity from 'velocity-animate'
import $ from 'jquery'

export default React.createClass({
  contextTypes: {
    panes: React.PropTypes.object,
  },

  childContextTypes: {
    pane: React.PropTypes.object
  },

  getChildContext() {
    const self = this
    return {
      pane: {
        setTitle: function(title) {
          self.setState({ title: title })
        },
        setWidth: function(width) {
          self.setState({ width: width })
        },
        setMinWidth: function(minWidth) {
          self.setState({ minWidth: minWidth })
        },
        setMaxWidth: function(maxWidth) {
          self.setState({ maxWidth: maxWidth })
        },
        setHeight: function(height) {
          self.setState({ height: height })
        },
        setClosable: function(closable) {
          self.setState({ closable: closable })
        },
        addDialog: function(id, element) {
          if (id == null || id == undefined) {
            id = Helper.guid()
          }
          self.state.dialogs.push({ id, element })
          self.setState({ dialogs: self.state.dialogs })
        },
        removeDialog: function(id) {
            var newItems = []
            for (var i = 0; i < self.state.dialogs.length; i++) {
              if (self.state.dialogs[i].id != id) {
                newItems.push(self.state.dialogs[i])
              }
            }
            self.state.dialogs = newItems
            self.setState({ dialogs: self.state.dialogs })
        },
        clearDialogs: function() {
            self.state.dialogs = []
            self.setState({ dialogs: self.state.dialogs })
        },
        close: function() {
          self.props.onClickClose()
        },
        scrollStart() {
          self.context.panes.scrollTop(0)
          self.context.panes.scrollLeft($(ReactDOM.findDOMNode(self)).offset().left)
        },
        scrollStartTop() {
          self.context.panes.scrollTop(0)
        },
        scrollStartLeft() {
          self.context.panes.scrollLeft($(ReactDOM.findDOMNode(self)).offset().left)
        }
      }
    }
  },

  getInitialState: function() {
    return {
      title: 'Title',
      width: 'auto',
      height: 'auto',
      minWidth: 300,
      minHeight: 300,
      maxWidth: undefined,
      maxHeight: undefined,
      closable: true,
      isResizable: {
        x: true,
        y: false,
        xy: false,
      },
      dialogs: [],
    }
  },

  render() {
    var dialog = []

    if (this.state.dialogs.length > 0) {
      var single = this.state.dialogs[this.state.dialogs.length - 1]
      dialog.push(
        <div key={single.id} className="dialog"><div className="object">{single.element}</div></div>
      )
    }

    return (
      <Resizable
        customClass="pane"
        isResizable={this.state.isResizable}
        width={this.state.width}
        height={this.state.height}
        minWidth={this.state.minWidth}
        minHeight={this.state.minHeight}
        maxWidth={this.state.maxWidth}
        maxHeight={this.state.maxHeight}
        customStyle={Object.assign({}, this.props.style, {
          minWidth: this.state.minWidth,
          minHeight: this.state.minHeight
        })}
        onResize={this.props.onResize}
        onResizeStart={this.props.onResizeStart}
        onResizeStop={this.props.onResizeStop}>
        <div className="panel"
          onMouseDown={this.props.onMouseDown}
          onTouchStart={this.props.onTouchStart}>
          {this.state.title}
        </div>
        {this.state.closable ? <div className="close" onClick={this.props.onClickClose}><span className="fa fa-remove"></span></div> : null}
        {this.props.children}
        <Anim
          component="div"
          willEnter={
            function(el, callback){
              el.hide()
              // el.attr('style', 'transform: rotateY(90deg)')
              Velocity(el, { opacity: 1.0, scale: 1 }, { display: "", duration: 150, easing: "swing", complete: callback })
            }
          }
          willAppear={
            function(el, callback){
              el.hide()
              // el.attr('style', 'transform: rotateY(90deg)')
              Velocity(el, { opacity: 1.0, scale: 1 }, { display: "", duration: 150, easing: "swing", complete: callback })
            }
          }
          willLeave={
            function(el, callback){
              Velocity(el, { opacity: 0.0, scale: 0.9 }, { display: "none", duration: 150, easing: "swing", complete: callback })
            }
          }>
          {dialog}
        </Anim>
      </Resizable>
    )
  },
})
