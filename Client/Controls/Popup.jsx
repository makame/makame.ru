import React from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery';
import Helper from '../Common/Helper.jsx'

export default React.createClass({

  contextTypes: {
    popupService: React.PropTypes.object,
  },

  getInitialState: function() {
    return {
      guid: Helper.guid()
    }
  },

  getDefaultProps: function() {
    return { on: 'hover', html: '', position: 'top center', popupClassName: '', offset: 9, delay: 300, tagName: 'div', activeClassName: 'active' }
  },

  show: function(e) {
    if (e) {
      e.stopPropagation()
      e.preventDefault()
    }

    var element = $(ReactDOM.findDOMNode(this))

    if (this.props.element) {
      element = $(ReactDOM.findDOMNode(this.props.element))
    }

    this.context.popupService.showPopup({html: this.props.html, guid: this.state.guid, className: this.props.popupClassName, offset: this.props.offset, element: element, position: this.props.position, elementActiveClassName: this.props.activeClassName})
  },

  hide: function(e) {
    if (e) {
      e.stopPropagation()
      e.preventDefault()
    }

    var delay = this.props.delay

    if (this.props.on == 'click') {
      delay = null
    }

    this.context.popupService.hidePopup(this.state.guid, delay)
  },

  // componentWillReceiveProps: function(nextProps) {
  //   this.props = nextProps
  //   if (nextProps.on) {
  //     if (nextProps.show) {
  //       this.show()
  //     }
  //     else {
  //       this.hide()
  //     }
  //   }
  //   this.setState({on: nextProps.on})
  // },

  componentDidMount: function() {
    this.setState({on: this.props.on})
  },

  render: function () {
    if (this.state.on == "manual") {
      return React.createElement(
        this.props.tagName || 'div',
        Object.assign({}, this.props, {
          className: this.props.className
        }),
        this.props.children
      )
    }
    else if (this.state.on == "click") {
      return React.createElement(
        this.props.tagName || 'div',
        Object.assign({}, this.props, {
          onClick: this.show,
          onTouchStart: this.show,
          className: this.props.className
        }),
        this.props.children
      )
    }
    else if (this.state.on == "hover") {
      return React.createElement(
        this.props.tagName || 'div',
        Object.assign({}, this.props, {
          onMouseOver: this.show,
          onMouseOut: this.hide,
          onTouchStart: this.show,
          onTouchEnd: this.hide,
          className: this.props.className
        }),
        this.props.children
      )
    }
    else {
      return null
    }
  }

})
