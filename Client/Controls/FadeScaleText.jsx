FadeScaleText = React.createClass({
  render() {
    const {className, children, ...props} = this.props

    return (
      <ChildsAnim duration={150}>
        {children ? (<div className={className} key="text">{children}</div>) : ''}
      </ChildsAnim>
    );
  }
})
