import React from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery';
import ReactTransitionGroup from 'react-addons-transition-group';

const AnimChild = React.createClass({

  componentWillAppear(callback) {
    if (this.props.willAppear) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.willAppear(el, callback)
    }
  },

  componentDidAppear() {
    if (this.props.didAppear) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.didAppear(el)
    }
  },

  componentWillEnter(callback) {
    if (this.props.willEnter) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.willEnter(el, callback)
    }
  },

  componentDidEnter() {
    if (this.props.didEnter) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.didEnter(el)
    }
  },

  componentWillLeave(callback) {
    if (this.props.willLeave) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.willLeave(el, callback)
    }
  },

  componentDidLeave() {
    if (this.props.didLeave) {
      var el = $(ReactDOM.findDOMNode(this))
      this.props.didLeave(el)
    }
  },

  render() {
    if (this.props.children) {
      return React.Children.only(this.props.children)
    }
    else {
      return null
    }
  }
})

export default class Anim extends React.Component {
  render() {
    const { className, component, ...props } = this.props

    var childCount = React.Children.count(this.props.children);
    var children = React.Children.map(this.props.children, function(child, idx) {
      return (
        <AnimChild
          {...props}>
          {child}
        </AnimChild>
      )
    }.bind(this));

    return (
      <ReactTransitionGroup
        component={component}
        className={className}
        transitionLeave={true}>
        {children}
      </ReactTransitionGroup>
    )
  }
}
