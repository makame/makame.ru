import React from 'react'
import ReactDOM from 'react-dom'

export default React.createClass({

  value() {
    return this.htmlEl.innerHTML
  },

  shouldComponentUpdate(nextProps) {
    return !this.htmlEl || nextProps.html !== this.htmlEl.innerHTML ||
    this.props.disabled !== nextProps.disabled;
  },

  componentDidUpdate() {
    if ( this.htmlEl && this.props.html !== this.htmlEl.innerHTML ) {
      this.htmlEl.innerHTML = this.props.html;
    }
  },

  onInput(evt) {
    if (this.props.onInput) {
      this.props.onInput(evt)
    }
    this.emitChange(evt)
  },

  onBlur(evt) {
    if (this.props.onBlur) {
      this.props.onBlur(evt)
    }
    this.emitChange(evt)
  },

  emitChange(evt) {
    if (!this.htmlEl) return;
    var html = this.htmlEl.innerHTML;
    if (this.props.onChange && html !== this.lastHtml) {
      evt.target = { value: html };
      this.props.onChange(evt);
    }
    this.lastHtml = html;
  },

  render() {
    return React.createElement(
      this.props.tagName || 'span',
      Object.assign({}, this.props, {
        ref: (e) => this.htmlEl = e,
        onInput: this.onInput,
        onBlur: this.onBlur,
        onKeyUp: this.props.onKeyUp,
        onKeyDown: this.props.onKeyDown,
        onKeyPress: this.props.onKeyPress,
        contentEditable: !this.props.disabled,
        dangerouslySetInnerHTML: {__html: this.props.html}
      }),
      this.props.children
    );
  }
})
