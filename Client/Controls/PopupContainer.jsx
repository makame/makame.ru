import React from 'react'
import ReactDOM from 'react-dom'
import Anim from '../Controls/Anim.jsx'
import $ from 'jquery'
import _ from 'underscore'
import Velocity from 'velocity-animate'

export default React.createClass({

  childContextTypes: {
    popupService: React.PropTypes.object
  },

  getChildContext: function() {
    return {
      popupService: this.state.popupService
    }
  },

  getInitialState: function() {
    const self = this

    return {
      popupService: {
        showPopup: function(popup) {
          self.setState({ popup: popup })
        },

        hidePopup: function(guid, delay) {
          clearTimeout(self.timeoutPopup)
          if (guid) {
            if (self.state.popup && self.state.popup.guid == guid) {
              self.delayPopup = delay
              if (!!self.delayPopup) {
                self.timeoutPopup = setTimeout(function(){
                  self.state.popup = null
                  self.setState({ popup: self.state.popup, element: null })
                }, self.delayPopup)
              }
              else {
                self.state.popup = null
                self.setState({ popup: self.state.popup, element: null })
              }
            }
          }
          else {
            self.delayPopup = null
            self.state.popup = null
            self.setState({ popup: self.state.popup, element: null })
          }
        },

        hideFront: function() {
          self.hideFront()
        },

        update: function() {
          self.componentDidUpdate()
        }
      },
      popup: null, padding_window: 10
    }
  },

  onClick() {
    this.state.popupService.hideFront()
  },

  handleResize: function(e) {
    this.state.popupService.hidePopup()
  },

  componentDidMount: function() {
    window.addEventListener('resize', this.handleResize)
  },

  componentWillUnmount: function() {
    window.removeEventListener('resize', this.handleResize)
  },

  timeoutPopup: null,
  delayPopup: null,

  stopDelayPopup() {
    clearTimeout(this.timeoutPopup)
  },

  restartDelayPopup() {
    var self = this
    if (self.delayPopup) {
      clearTimeout(self.timeoutPopup)
      self.timeoutPopup = setTimeout(function(){
        self.state.popup = null
        self.setState({ popup: self.state.popup, element: null })
      }, self.delayPopup)
    }
  },

  hidePopup: function(e) {
    if (this.state.popup.element && this.state.popup.elementActiveClassName) {
      this.state.popup.element.removeClass(this.state.popup.elementActiveClassName)
    }
    this.state.popup = null
    this.setState({popup: this.state.popup, element: null})
  },

  componentDidUpdate: function() {
    var container = $(ReactDOM.findDOMNode(this))

    if (this.state.popup) {
      var popup = $(ReactDOM.findDOMNode(this.refs.popup))

      if (this.state.popup.element && this.state.popup.elementActiveClassName) {
        this.state.popup.element.addClass(this.state.popup.elementActiveClassName)
      }

      if (popup) {
        var element = this.state.popup.element

        var min_x = _.max([$(window).scrollLeft(), container.offset().left])
        var min_y = _.max([$(window).scrollTop(), container.offset().top])

        var max_x = _.min([$(window).width() + $(window).scrollLeft(), container.offset().left + container.outerWidth()])
        var max_y = _.min([$(window).height() + $(window).scrollTop(), container.offset().top + container.outerHeight()])

        var this_y = element.offset().top
        var this_x = element.offset().left

        var this_height = element.outerHeight()
        var this_width = element.outerWidth()

        var this_y_middle = this_y + this_height / 2
        var this_x_middle = this_x + this_width / 2

        var popup_height = popup.outerHeight()
        var popup_width = popup.outerWidth()

        var top = this_y
        var left = this_x

        var want_y = 'auto'
        var want_x = 'auto'

        var result_y = 'auto'
        var result_x = 'auto'
        var result_alter_y = 'center'

        if (this.state.popup.position == 'top center' || this.state.popup.position == 'top') {
          top = this_y - popup_height - this.state.popup.offset
          left = this_x + this_width / 2 - popup_width / 2

          result_y = "top"
          result_x = "center"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = this_x - popup_width - this.state.popup.offset
            result_x = "left"
          }
          else if (left < min_x + this.state.padding_window) {
            left = this_x + this_width + this.state.padding_window
            result_x = "right"
          }

          if (top < this_height + this.state.padding_window) {
            top = this_y + this_height + this.state.popup.offset
            result_y = "bottom"
          }
        }

        if (this.state.popup.position == 'top left') {
          top = this_y - popup_height - this.state.popup.offset
          left = this_x - popup_width + 22 + this_width / 2

          result_y = "top"
          result_x = "left"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = this_x - popup_width - this.state.popup.offset
            result_x = "left"
          }
          else if (left < min_x + this.state.padding_window) {
            left = this_x + this_width + this.state.padding_window
            result_x = "right"
          }

          if (top < this_height - this.state.padding_window) {
            top = this_y + this_height + this.state.popup.offset
            result_y = "bottom"
          }
        }

        if (this.state.popup.position == 'top right') {
          top = this_y - popup_height - this.state.popup.offset
          left = this_x + this_width - 22 - this_width / 2

          result_y = "top"
          result_x = "right"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = this_x - popup_width - this.state.popup.offset
            result_x = "left"
          }
          else if (left < min_x + this.state.padding_window) {
            left = this_x + this_width + this.state.padding_window
            result_x = "right"
          }

          if (top < this_height - this.state.padding_window) {
            top = this_y + this_height + this.state.popup.offset
            result_y = "bottom"
          }
        }

        if (this.state.popup.position == 'bottom center' || this.state.popup.position == 'bottom') {
          top = this_y + this_height + this.state.popup.offset
          left = this_x + this_width / 2 - popup_width / 2

          result_y = "bottom"
          result_x = "center"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = this_x - popup_width - this.state.popup.offset
            result_x = "left"
          }
          else if (left < min_x + this.state.padding_window) {
            left = this_x + this_width + this.state.padding_window
            result_x = "right"
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.popup.offset
            result_y = "top"
          }
        }

        if (this.state.popup.position == 'bottom left') {
          top = this_y + this_height + this.state.popup.offset
          left = this_x - popup_width + 22 + this_width / 2

          result_y = "bottom"
          result_x = "left"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = this_x - popup_width - this.state.popup.offset
            result_x = "left"
          }
          else if (left < min_x + this.state.padding_window) {
            left = this_x + this_width + this.state.padding_window
            result_x = "right"
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.popup.offset
            result_y = "top"
          }
        }

        if (this.state.popup.position == 'bottom right') {
          top = this_y + this_height + this.state.popup.offset
          left = this_x + this_width - 22 - this_width / 2

          result_y = "bottom"
          result_x = "right"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = this_x - popup_width - this.state.popup.offset
            result_x = "left"
          }
          else if (left < min_x + this.state.padding_window) {
            left = this_x + this_width + this.state.padding_window
            result_x = "right"
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.popup.offset
            result_y = "top"
          }
        }

        if (this.state.popup.position == 'left') {
          top = this_y - popup_height / 2 + this_height / 2
          left = this_x - popup_width - this.state.popup.offset

          result_y = "center"
          result_x = "left"
          result_alter_y = "center"

          if (left < min_x + this.state.padding_window) {
            left = this_x + this_width + this.state.popup.offset
            result_x = "right"
          }

          // if (top < this_height + this.state.padding_window) {
          //   top = this_y - this_height + this.state.padding_window
          //   result_alter_y = "bottom"
          // }
          // else if (top + popup_height > max_y - this.state.padding_window) {
          //   top = this_y - popup_height + this_height - this.state.padding_window
          //   result_alter_y = "top"
          // }
        }

        if (this.state.popup.position == 'right') {
          top = this_y - popup_height / 2 + this_height / 2
          left = this_x + this_width + this.state.popup.offset

          result_y = "center"
          result_x = "right"
          result_alter_y = "center"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = this_x - popup_width - this.state.popup.offset
            result_x = "left"
          }

          // if (top < this_height + this.state.padding_window) {
          //   top = this_y - this_height + this.state.padding_window
          //   result_alter_y = "bottom"
          // }
          // else if (top + popup_height > max_y - this.state.padding_window) {
          //   top = this_y - popup_height + this_height - this.state.padding_window
          //   result_alter_y = "top"
          // }
        }

        if (result_y == 'bottom') {
          var left_popup = this_x_middle - left - 8

          left_popup = _.min([left_popup, popup_width - 20])
          left_popup = _.max([left_popup, 20])

          var barrow = popup.find('.barrow').addClass('bottom')
          barrow.css("left", (left_popup - 1) + "px")
          var farrow = popup.find('.farrow').addClass('bottom')
          farrow.css("left", (left_popup) + "px")
        }
        else if (result_y == 'top') {
          var left_popup = this_x_middle - left - 8

          left_popup = _.min([left_popup, popup_width - 20])
          left_popup = _.max([left_popup, 20])

          var barrow = popup.find('.barrow').addClass('top')
          barrow.css("left", (left_popup - 1) + "px")
          var farrow = popup.find('.farrow').addClass('top')
          farrow.css("left", (left_popup) + "px")
        }
        else if (result_y == 'center') {
          if (result_x == 'left') {
            var top_popup = this_y_middle - top - 8

            top_popup = _.min([top_popup, popup_height - 20])
            top_popup = _.max([top_popup, 20])

            var barrow = popup.find('.barrow').addClass('left')
            barrow.css("top", (top_popup - 5) + "px")
            var farrow = popup.find('.farrow').addClass('left')
            farrow.css("top", (top_popup - 4) + "px")
          }
          else if (result_x == 'right') {
            var top_popup = this_y_middle - top - 8

            top_popup = _.min([top_popup, popup_height - 20])
            top_popup = _.max([top_popup, 20])

            var barrow = popup.find('.barrow').addClass('right')
            barrow.css("top", (top_popup - 5) + "px")
            var farrow = popup.find('.farrow').addClass('right')
            farrow.css("top", (top_popup - 4) + "px")
          }
        }

        popup.offset({top: top, left: left})
      }
    }
  },

  hideFront() {
    if (this.state.popup) {
      this.hidePopup()
    }
  },

  stopPropagation(e) {
    if (e) {
      e.stopPropagation()
      e.preventDefault()
    }
  },

  render() {
    var child = []

    if (this.state.popup) {
      var finalClassName = "popup"
      if (this.state.popup.className) {
        finalClassName += " " + this.state.popup.className
      }
      child.push(<div ref="popup" className={finalClassName} key={this.state.popup.guid} onMouseOver={this.stopDelayPopup} onMouseOut={this.restartDelayPopup}>{this.state.popup.html}<div className="farrow" ref="farrow"></div></div>)
    }

    return (
      <div>
        <div onClick={this.onClick}>
          {this.props.children}
        </div>
        <Anim
          component="div"
          willEnter={
            function(el, callback){
              el.hide()
              // Velocity(el, { opacity: 0.0, scale: 1.0 }, { display: "none", duration: 0 })
              Velocity(el, { opacity: [1, 0], scale: [1, 0.9] }, { display: "", duration: 150, easing: "swing", complete: callback })
            }
          }
          willAppear={
            function(el, callback){
              el.hide()
              // Velocity(el, { opacity: 0.0, scale: 1.0 }, { display: "none", duration: 0 })
              Velocity(el, { opacity: [1, 0], scale: [1, 0.9] }, { display: "", duration: 150, easing: "swing", complete: callback })
            }
          }
          willLeave={
            function(el, callback){
              Velocity(el, { opacity: 0.0, scale: 0.9 }, { display: "none", duration: 150, easing: "swing", complete: callback })
            }
          }>
          {child}
        </Anim>
      </div>
    )
  }
})
