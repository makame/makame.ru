import React from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery';
import _ from 'underscore';
import ContentEditable from './ContentEditable.jsx'

export default React.createClass({

  getInitialState() {
    return {
      value: ""
    }
  },

  getDefaultProps() {
    return {
      linkState: null
    }
  },

  handleChange(e) {
    this.state.value = this.calcHtml(this.refs.input.value())
    this.setState({value: this.state.value})

    if (this.props.linkState) {
      this.props.linkState.requestChange(this.state.value)
    }
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.linkState) {
      this.setState({value: nextProps.linkState.value})
    }
  },

  componentDidMount() {
    if (this.props.linkState) {
      this.setState({value: this.props.linkState.value})
    }
  },

  calcHtml(value, setSelection) {
    var self = this

    var result = "";

    $('<div/>').append($.parseHTML(value)).contents().each(function(index, itemRow) {
      var jitemRow = $(itemRow)
      var tagRow = jitemRow.prop("tagName")

      if (tagRow == 'DIV' || tagRow == 'SPAN' || tagRow == 'A') {
        result += self.getContent(self, jitemRow)
      }
      else {
        result += $('<div/>').append(jitemRow.clone()).html()
      }
    })

    result = result.replace(/<br>/g, '')

    return result
  },

  getContent(self, jitemRow) {
    var result = "";

    jitemRow.contents().each(function(index, item) {
      var jitem = $(item)
      var tag = jitem.prop("tagName")

      if (tag == 'DIV' || tag == 'SPAN' || tag == 'A') {
        result += self.getContent(self, jitem)
      }
      else {
        result += $('<div/>').append(jitem.clone()).html()
      }
    })

    return result
  },

  onKeyDown(e) {
  },

  onKeyUp(e) {
    if (e.which == 13) {
      if (this.props.onEnter) {
        this.props.onEnter(this)
      }
    }
    else if (e.which == 8) {
      if (this.props.onBack) {
        this.props.onBack(this)
      }
    }
    else if (e.which == 37) {
      if (this.props.onLeft) {
        this.props.onLeft(this)
      }
    }
    else if (e.which == 39) {
      if (this.props.onRight) {
        this.props.onRight(this)
      }
    }
  },

  value() {
    return this.state.value
  },

  render() {
    var self = this
    var html = this.state.value

    // console.log(html);

    return (
      <ContentEditable placeholder={this.props.placeholder} className={"editor-simple " + this.props.className} ref="input" html={html} disabled={this.props.disabled} onChange={this.handleChange} onKeyDown={this.onKeyDown} onKeyUp={this.onKeyUp} />
    );
  }
})
