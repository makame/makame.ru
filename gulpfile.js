var gulp = require('gulp');  // Base gulp package
var gulpSequence = require('gulp-sequence');
var babelify = require('babelify'); // Used to convert ES6 & JSX to ES5
var browserify = require('browserify'); // Providers "require" support, CommonJS
var notify = require('gulp-notify'); // Provides notification to both the console and Growel
var rename = require('gulp-rename'); // Rename sources
var sourcemaps = require('gulp-sourcemaps'); // Provide external sourcemap files
var gutil = require('gulp-util'); // Provides gulp utilities, including logging and beep
var chalk = require('chalk'); // Allows for coloring for logging
var source = require('vinyl-source-stream'); // Vinyl stream support
var buffer = require('vinyl-buffer'); // Vinyl stream support
var duration = require('gulp-duration'); // Time aspects of your gulp process
var less = require('gulp-less');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var plumber = require('gulp-plumber');

var FtpDeploy = require('ftp-deploy');
var Config = require('./Config/Config.js')

// Configuration for Gulp
var config = {
  js: {
    src: './Client/Router.jsx',
    js: './Client/**/*',
    srcDir: './Client/',
    outputDir: './Public/',
    outputFile: 'client.js',
  },
  less: {
    src: './Less/style.less',
    less: './Less/**/*.less',
    sass: './Less/**/*.sass',
    scss: './Less/**/*.scss',
    srcDir: './Less/',
    outputDir: './Public/',
    outputFile: 'style.css',
  }
};

// Error reporting function
function mapError(err) {
  if (err.fileName) {
    // Regular error
    gutil.log(chalk.red(err.name)
    + ': ' + chalk.yellow(err.fileName.replace(__dirname + '/src/js/', ''))
    + ': ' + 'Line ' + chalk.magenta(err.lineNumber)
    + ' & ' + 'Column ' + chalk.magenta(err.columnNumber || err.column)
    + ': ' + chalk.blue(err.description));
  } else {
    // Browserify error..
    gutil.log(chalk.red(err.name)
    + ': '
    + chalk.yellow(err.message));
  }
}

// Compile SASS from Sass files
gulp.task('sass', function() {
  return gulp.src([config.less.sass, config.less.scss])
  .pipe(plumber())
  .pipe(sass().on("error", sass.logError))
  .pipe(gulp.dest(config.less.srcDir)) // Set the output folder
  .pipe(notify({
    message: 'Generated file: <%= file.relative %>',
  }))
});

// Compile LESS from Sass files
gulp.task('less', function() {
  return gulp.src(config.less.src)
  .pipe(plumber())
  .on('error', sass.logError) // Map error reporting
  .pipe(buffer()) // Convert to gulp pipeline
  .pipe(less())
  .pipe(rename(config.less.outputFile))
  .pipe(sourcemaps.init({loadMaps: true})) // Extract the inline sourcemaps
  .pipe(sourcemaps.write('./map')) // Set folder for sourcemaps to output to
  .pipe(gulp.dest(config.less.outputDir))
  .pipe(livereload()) // Set the output folder
  .pipe(notify({
    message: 'Generated file: <%= file.relative %>',
  }))
});

// Gulp task for build
gulp.task('mode_dev', function() {
  return gulp.src('./Config/dev_mode.js').pipe(buffer()).pipe(rename('mode.js')).pipe(gulp.dest('./Config/'))
  .pipe(notify({
    message: 'Mode is DEVELOP',
  }))
});

// Gulp task for build
gulp.task('mode_rel', function() {
  return gulp.src('./Config/rel_mode.js').pipe(buffer()).pipe(rename('mode.js')).pipe(gulp.dest('./Config/'))
  .pipe(notify({
    message: 'Mode is PRODUCTIVE',
  }))
});

gulp.task('scripts', function() {
  var bundleTimer = duration('Javascript bundle time');

  return browserify(config.js.src, { debug: true }) // Browserify
  .transform(babelify, {presets: ['es2015', 'react', 'stage-0']})
  .bundle()
  .pipe(plumber())
  .on('error', mapError) // Map error reporting
  .pipe(source('main.jsx')) // Set source name
  .pipe(buffer()) // Convert to gulp pipeline
  .pipe(rename(config.js.outputFile)) // Rename the output file
  .pipe(sourcemaps.init({loadMaps: true})) // Extract the inline sourcemaps
  .pipe(sourcemaps.write('./map')) // Set folder for sourcemaps to output to
  .pipe(gulp.dest(config.js.outputDir))
  .pipe(livereload()) // Set the output folder
  .pipe(notify({
    message: 'Generated file: <%= file.relative %>',
  })) // Output the file being created
  .pipe(bundleTimer) // Output time timing of the file creation
});

gulp.task('deploy', function(callback) {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write('Uploading...')

    var ftpDeploy = new FtpDeploy();

    var config = {
      username: Config.ftp_username,
      password: Config.ftp_password,
      host: Config.ftp_host,
      port: Config.ftp_port,
      localRoot: __dirname,
      remoteRoot: Config.ftp_root,
      exclude: ['.git', '.gitignore', 'README.txt', '**/node_modules/**/*', '**/bower_components/**/*', 'gulpfile.js', 'App/*', 'Client/*', 'Less/*', '*.jsx', '*.less', '*.scss', '*.sass']
    }

    ftpDeploy.deploy(config, function(err) {
      if (err)
      {
        process.stdout.clearLine();
        process.stdout.cursorTo(0);
        console.log(err)
      }
      else
      {
        process.stdout.clearLine();
        process.stdout.cursorTo(0);
        console.log('Finished uploading');
      }
      callback();
    });

    ftpDeploy.on('uploading', function(data) {
      process.stdout.clearLine();
      process.stdout.cursorTo(0);
      process.stdout.write('Uploading: ' + chalk.green(data.percentComplete + '%'))
      // data.totalFileCount;       // total file count being transferred
      // data.transferredFileCount; // number of files transferred
      // data.percentComplete;      // percent as a number 1 - 100
      // data.filename;             // partial path with filename being uploaded
    });

    // ftpDeploy.on('uploaded', function(data) {
    // 	console.log(data);         // same data as uploading event
    // });
});

// Default Task
gulp.task('dev', function(callback) {
  gulpSequence('sass', 'less', 'mode_dev', 'scripts')(callback)
});

// Default Task
gulp.task('devcss', function(callback) {
  gulpSequence('sass', 'less')(callback)
});

// Default Task
gulp.task('devjs', function(callback) {
  gulpSequence('mode_dev', 'scripts')(callback)
});

// Watch for changes in files
gulp.task('watch', function() {
  // Watch .js files
  gulp.watch([config.less.sass, config.less.scss, config.less.less], ['devcss']);
  // Watch image files
  gulp.watch(config.js.js, ['devjs']);
});

// Default Task
gulp.task('default', function(callback) {
  livereload.listen();
  gulpSequence('dev', 'watch')(callback)
});

// Default Task
gulp.task('release', function(callback) {
  gulpSequence('sass', 'less', 'mode_rel', 'scripts', 'deploy')(callback)
});
