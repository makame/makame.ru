'use strict';

import _ from 'underscore';
import React, {
  Component,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  RefreshControl,
  AlertIOS,
  StatusBarIOS,
  TouchableHighlight,
  TouchableWithoutFeedback,
  WebView,
} from 'react-native';

var Animatable = require('react-native-animatable');

import WebViewFill from '../Controls/WebViewFill.js';
import Button from '../Controls/Button.js';

import Config from '../Config.js';

export default React.createClass({

  // contextTypes: {
  //   blogs: React.PropTypes.object,
  // },
  //
  // getContext: function() {
  //   return {
  //     blogs: ['hello! Test']
  //   }
  // },

  getInitialState: function(){
    const self = this

    return {
      blog: {},

      loadBlog: function(id, callback) {
        const dataReq = {
          id: id,
        }

        fetch(Config.url + "get_blog", {
          method: "POST",
          body: JSON.stringify(dataReq),
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
        })
        .then((response) => response.json())
        .then((responseData) => {
          const data = responseData

          if (callback) {
            callback()
          }

          self.setState({blog: data})

          // AlertIOS.alert(
          //   "POST Response",
          //   "Response Body -> " + JSON.stringify(responseData.body)
          // )
        })
        .catch((error) => {
          console.warn(JSON.stringify(error));
          if (callback) {
            callback(error)
          }
        })
        .done();
      },
    };
  },

  childContextTypes: {
    blogs: React.PropTypes.object
  },

  getChildContext: function() {
    return {
      blogs: this.state.blogs
    }
  },

  isReadyAjax: false,
  isReadyComments: false,

  componentDidMount() {
    const self = this
    // this.setState({isRefreshing: true});
    StatusBarIOS.setNetworkActivityIndicatorVisible(true)
    this.state.loadBlog(this.props.route.id, function() {
      StatusBarIOS.setNetworkActivityIndicatorVisible(false)
      // self.setState({
      //   isRefreshing: false,
      // });
      self.isReadyAjax = true
      if (self.isReadyComments) {
        self.refs.loading.fadeOut(500).then((endState) => {self.refs.loading.setNativeProps({pointerEvents: 'none'})});
      }
    })
  },

  _onReadyComments() {
    const self = this
    self.isReadyComments = true
    if (self.isReadyAjax) {
      self.refs.loading.fadeOut(500).then((endState) => {self.refs.loading.setNativeProps({pointerEvents: 'none'})});
    }
  },

  _onRefresh() {
    const self = this
    this.setState({isRefreshing: true});
    StatusBarIOS.setNetworkActivityIndicatorVisible(true)
    this.state.loadBlog(this.props.route.id, function() {
      StatusBarIOS.setNetworkActivityIndicatorVisible(false)
      self.setState({
        isRefreshing: false,
      });
    })
  },

  render() {
    const self = this

    var image = {uri: this.state.blog.image}
    if (!this.state.blog.image || this.state.blog.image == "") {
      image = null
    }

    var text = _.map(this.state.blog.preview, function(row, index) {
      return (
        <Text key={index}>
          {_.map(row.items, function(item) {
            return item.text
          }).join()}
        </Text>
      )
    })

    return (
      <View style={{flex: 1}}>
        <ScrollView style={{overflow: 'visible', flex: 1}}
          refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={this._onRefresh}
              tintColor="#aaa"
              title="Updating..."
              colors={['#ff0000', '#00ff00', '#0000ff']}
              progressBackgroundColor="#ffff00"
              />
          }
          scrollEnabled={true}
          removeClippedSubviews={false}
          horizontal={false}
          automaticallyAdjustContentInsets={true}
          contentInset={{bottom:49}}
          onScroll={(e) => {
            // console.warn(JSON.stringify(e.nativeEvent, {indent: true}));
            if (e.nativeEvent.contentOffset.y > 1) {
              StatusBarIOS.setHidden(true, 'slide')
            }
            else {
              StatusBarIOS.setHidden(false, 'slide')
            }
          }}
          bounces={true}
          centerContent={false}
          scrollsToTop={true}>
          <View style={{flex: 1, backgroundColor: '#fff'}}>
            <View style={styles.blogContainer} collapsable={false}>
              <Text style={styles.title}>
                {this.state.blog.title}
              </Text>
              {
                image ? (
                  <Image
                    style={styles.image}
                    source={image}
                    defaultSource={require("../preloader.gif")}
                    onLoadStart={(e) => self.setState({loading: true})}
                    onError={(e) => {console.log(JSON.stringify(e)); self.setState({loading: false})}}
                    onProgress={(e) => self.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
                    onLoad={() => self.setState({loading: false})}
                    />
                ) : null
              }
              <View style={styles.description}>
                {text}
              </View>

              <WebViewFill source={{url: Config.url + 'api/blog/comments/' + this.props.route.id}} onReady={this._onReadyComments}>
              </WebViewFill>
            </View>
          </View>
        </ScrollView>
        <Animatable.View ref="loading" style={{flex: 1, position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, backgroundColor: '#111'}}></Animatable.View>
      </View>
    )
  }
})

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    paddingBottom: 20,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  welcome: {
    flex: 1,
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'stretch',
    padding: 10,
  },
  instructions: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  blogContainer: {
    flex: 1,
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'stretch',
    // backgroundColor: '#f5f5f5',
  },
  title: {
    flex: 1,
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'stretch',
    padding: 10,
  },
  image: {
    flex: 1,
    height: 150,
    overflow: 'hidden',
    alignItems: 'stretch',
    paddingTop: 10,
  },
  description: {
    flex: 1,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 5,
  },
});
