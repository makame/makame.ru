'use strict';

import _ from 'underscore';
import React, {
  Component,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  RefreshControl,
  AlertIOS,
  StatusBarIOS,
  TouchableHighlight,
  WebView,
} from 'react-native';

var Animatable = require('react-native-animatable');
import WebViewFill from '../Controls/WebViewFill.js';
import Button from '../Controls/Button.js';
import Blog from './Blog.js';
import SlideMenu from '../Controls/SlideMenu.js';

import Config from '../Config.js';

export default React.createClass({

  // contextTypes: {
  //   blogs: React.PropTypes.object,
  // },
  //
  // getContext: function() {
  //   return {
  //     blogs: ['hello! Test']
  //   }
  // },

  getInitialState: function(){
    return {
      list: [],
      count: 0,

      limit: 0,
      per: 4,
    };
  },

  initBlog: function() {
    const self = this

    self.state.list = []
    self.state.limit = 0
    self.state.per = 4
  },

  loadMore: function(callback) {
    const self = this

    const dataReq = {
      limit: self.state.per,
      skip: self.state.limit,
    }

    fetch(Config.url + "get_blogs", {
      method: "POST",
      body: JSON.stringify(dataReq),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
    .then((response) => response.json())
    .then((responseData) => {
      const data = responseData

      _.each(data.items, function(item) {
        var find = _.find(self.state.list, function(kit){return item.id == kit.id})
        if (find) {
          find.title = item.title
          find.image = item.image
          find.date = new Date(Date.parse(item.date))
          find.image = item.image
          find.icon = item.icon
          find.preview = item.preview
        }
        else {
          self.state.list.push(
            {
              id: item.id,
              title: item.title,
              image: item.image,
              date: new Date(Date.parse(item.date)),
              icon: item.icon,
              preview: item.preview,
            }
          )
        }
      })

      if (callback) {
        callback()
      }

      self.state.limit = self.state.limit + self.state.per
      self.state.count = data.count

      self.setState({limit: self.state.limit, count: self.state.count, per: self.state.per, list: self.state.list})

      // AlertIOS.alert(
      //   "POST Response",
      //   "Response Body -> " + JSON.stringify(responseData.body)
      // )
    })
    .catch((error) => {
      console.warn(JSON.stringify(error));
      if (callback) {
        callback(error)
      }
    })
    .done();
  },

  componentDidMount() {
    const self = this
    this.initBlog()
    StatusBarIOS.setNetworkActivityIndicatorVisible(true)
    this.loadMore(function() {
      StatusBarIOS.setNetworkActivityIndicatorVisible(false)
      self.refs.loading.fadeOut(500).then((endState) => {self.refs.loading.setNativeProps({pointerEvents: 'none'})});
    })
  },

  _loadMore() {
    const self = this
    this.setState({isLoading: true});
    StatusBarIOS.setNetworkActivityIndicatorVisible(true)
    this.loadMore(function() {
      StatusBarIOS.setNetworkActivityIndicatorVisible(false)
      self.setState({
        isLoading: false,
      });
    })
  },

  _onRefresh() {
    const self = this
    this.setState({isRefreshing: true});
    this.initBlog()
    StatusBarIOS.setNetworkActivityIndicatorVisible(true)
    this.loadMore(function() {
      StatusBarIOS.setNetworkActivityIndicatorVisible(false)
      self.setState({
        isRefreshing: false,
      });
    })
  },

  render() {
    const self = this

    var items = []

    _.each(this.state.list, function(blog, index) {

      var image = {uri: blog.image}
      if (!blog.image || blog.image == "") {
        image = null
      }

      var text = _.map(blog.preview, function(row, index) {
        return (
          <Text key={index}>
            {_.map(row.items, function(item) {
              return item.text
            }).join()}
          </Text>
        )
      })

      items.push(
        <View style={styles.blogContainer} key={blog.id} collapsable={false}>
          <Text style={styles.title}>
            {blog.title}
          </Text>
          {
            image ? (
              <Image
                style={styles.image}
                source={image}
                defaultSource={require("../preloader.gif")}
                onLoadStart={(e) => self.setState({loading: true})}
                onError={(e) => {console.log(JSON.stringify(e)); self.setState({loading: false})}}
                onProgress={(e) => self.setState({progress: Math.round(100 * e.nativeEvent.loaded / e.nativeEvent.total)})}
                onLoad={() => self.setState({loading: false})}
                />
            ) : null
          }
          <View style={styles.description}>
            {text}
          </View>
          <Button onPress={()=>{
              self.props.navigator.push({
                name: 'Blog',
                component: Blog,
                id: blog.id
              })
            }}>
            Read Now
          </Button>
        </View>
      )
    })

    if (self.state.limit < self.state.count) {
      items.push(
        <Button onPress={this._loadMore} busy={self.state.isLoading} key="loadMOre">
          Load More
        </Button>
      )
    }

    return (
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <Text style={styles.welcome}>
            Welcome to Makame Blog!
          </Text>
          <Text style={styles.instructions}>
            #Programming #Info #Motivation
          </Text>
        </View>
        <ScrollView style={{overflow: 'visible', flex: 1}}
          refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={this._onRefresh}
              tintColor="#aaa"
              title="Updating..."
              colors={['#ff0000', '#00ff00', '#0000ff']}
              progressBackgroundColor="#ffff00"
              />
          }
          scrollEnabled={true}
          removeClippedSubviews={false}
          horizontal={false}
          automaticallyAdjustContentInsets={false}
          contentInset={{bottom:49}}
          onScroll={(e) => {
            // console.warn(JSON.stringify(e.nativeEvent, {indent: true}));
            if (e.nativeEvent.contentOffset.y > 1) {
              StatusBarIOS.setHidden(true, 'slide')
            }
            else {
              StatusBarIOS.setHidden(false, 'slide')
            }
          }}
          bounces={true}
          centerContent={false}
          scrollsToTop={true}>
          <View style={{flex: 1, backgroundColor: '#fff'}}>
            {items}
          </View>
        </ScrollView>
        <Animatable.View ref="loading" style={{flex: 1, position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, backgroundColor: '#000'}}></Animatable.View>
      </View>
    )
  }
})

const styles = StyleSheet.create({
  container: {
    paddingTop: 40,
    paddingBottom: 20,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  welcome: {
    flex: 1,
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'stretch',
    padding: 10,
  },
  instructions: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  blogContainer: {
    flex: 1,
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'stretch',
    // backgroundColor: '#f5f5f5',
  },
  title: {
    flex: 1,
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'stretch',
    padding: 10,
  },
  image: {
    flex: 1,
    height: 150,
    overflow: 'hidden',
    alignItems: 'stretch',
    paddingTop: 10,
  },
  description: {
    flex: 1,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 5,
  },
});
