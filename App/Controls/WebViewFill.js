'use strict';

import React, {
  WebView,
} from 'react-native';

export default React.createClass({

  getInitialState: function(){
    return {
      webViewHeight: 0,
      ready: false,
    };
  },

  _updateWebViewHeight(event) {
    if (parseInt(event.jsEvaluationValue) > 100) {
      this.setState({webViewHeight: parseInt(event.jsEvaluationValue)});
    }
  },

  _onLoad() {
    if (this.props.onReady && !this.state.ready) {
      this.setState({ready: true});
      this.props.onReady()
    }
  },

  render() {
    return (
      <WebView
      style={{
        flex: 1,
        height: this.state.webViewHeight,
      }}
      onLoad={this._onLoad}
      onNavigationStateChange={this._updateWebViewHeight}
      scalesPageToFit={true}
      injectedJavaScript="document.body.scrollHeight;"
      scrollEnabled={false}
      automaticallyAdjustContentInsets={true}
      {...this.props}
      ></WebView>
    )
  }
})
