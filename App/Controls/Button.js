'use strict';

import React, {
  Text,
  View,
  TouchableWithoutFeedback,
} from 'react-native';

var Animatable = require('react-native-animatable');

export default React.createClass({

  getInitialState: function(){
    const self = this

    return {
      busy: false,
    };
  },

  render() {
    const self = this

    const {className, ...props} = this.props
console.log(props);

    return (
      <TouchableWithoutFeedback onPress={this.props.onPress} onPressIn={() => {self.refs.button.transitionTo({backgroundColor: "rgb(54, 164, 255)", borderColor: "rgba(54, 164, 255, 1)", color: '#fff'}, 200);self.refs.button_container.transitionTo({translateY: 2}, 200);}} onPressOut={() => {self.refs.button.transitionTo({backgroundColor: "rgba(54, 164, 255, 0.0)", borderColor: self.state.isLoading ? '#ddd' : '#aaa', color: self.state.isLoading ? '#ddd' : '#333'}, 200);self.refs.button_container.transitionTo({translateY: 0}, 200);}}>
        <View style={{padding: 10}}>
          <Animatable.View ref="button_container" style={{shadowColor: '#000', transform: [{translateY: 0}], shadowRadius: 1, shadowOpacity: 0.2, borderRadius: 5, shadowOffset: {widht: 0, height: 1}}}>
            <Animatable.Text style={{padding: 10, fontWeight: '600', borderRadius: 5, overflow: 'hidden', shadowColor: '#000', shadowRadius: 5, shadowOpacity: 0.2, borderColor: self.state.isLoading ? '#ddd' : '#aaa', borderWidth: 1, fontSize: 12, flex: 1, textAlign: 'center', color: self.state.isLoading ? '#ddd' : '#333', backgroundColor: "rgba(54, 164, 255, 0.0)"}} ref="button">
              {this.props.children}
            </Animatable.Text>
          </Animatable.View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
})
