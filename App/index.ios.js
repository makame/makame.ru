/**
* Sample React Native App
* https://github.com/facebook/react-native
*/
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator
} from 'react-native';

var Dimensions = require('Dimensions');
var windowSize = Dimensions.get('window');

import Blogs from './Modeling/Blogs.js';
import Blog from './Modeling/Blog.js';
import SlideMenu from './Controls/SlideMenu.js';

class makameBlog extends Component {
  render() {
    return (
      <Navigator
        initialRoute={{name: 'Blogs', component: Blogs}}
        configureScene={() => {
          return Object.assign({}, Navigator.SceneConfigs.FloatFromRight, {
            springTension: 100,
            springFriction: 1,
            gestures: {
              pop: Object.assign({}, Navigator.SceneConfigs.FloatFromRight.gestures.pop, {
                snapVelocity: 8,
                edgeHitWidth: windowSize.width
              })
            }
          });
        }}
        renderScene={(route, navigator) => {
          if (route.component) {
            return <SlideMenu
              renderLeftView = {() => <View><Text>jhghjhg</Text></View>}
              renderCenterView = {() => React.createElement(route.component, { route, navigator })} />
          }
        }}
        />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    height: windowSize.height,
  },
});

AppRegistry.registerComponent('makameBlog', () => makameBlog);
