var is_dev = false;

process.argv.forEach(function (val, index, array) {
  if (val == '-d') {
    is_dev = true
  }
});

if (is_dev) {
  Config = require('./Config/Config_Dev.js')
}
else {
  Config = require('./Config/Config.js')
}

Config.is_dev = is_dev

var express = require('express')
var app = express()
var http = require('http')
var _ = require('underscore')
var fs = require('fs')
var stringify = require('node-stringify')
var bodyParser = require('body-parser')
Sequelize = require('sequelize')
var cookieParser = require('cookie-parser')
var session = require('express-session')
var MySQLStore = require('express-mysql-session')(session);
var passport = require('passport')
var FacebookStrategy = require('passport-facebook').Strategy;
var requireDir = require('require-dir')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())

sequelize = new Sequelize(Config.db_database, Config.db_username, Config.db_password, {
  host: Config.db_host,
  dialect: 'mysql',
  port: Config.db_port,
});

requireDir('./Models')

passport.use('facebook', new FacebookStrategy({
  clientID: Config.facebook_api_key,
  clientSecret: Config.facebook_api_secret,
  callbackURL: Config.url + Config.callback_url
}, function(access_token, refresh_token, profile, done) {
  process.nextTick(function() {
    User.findOne({where: {'facebook': profile.id}}).then(function(user) {
      if (user)
      {
        return done(null, user);
      }
      else
      {
        var newUser = {};
        newUser.facebook = profile.id;
        newUser.access_token = access_token;
        // newUser.email = profile.emails[0].value;
        newUser.username = profile.displayName;
        User.upsert(newUser).then(function (userDB) {
          return done(null, newUser);
        });
      }
    });
  });
}));

passport.serializeUser(function(user, done) {
  done(null, JSON.stringify(user));
});

passport.deserializeUser(function(obj, done) {
  done(null, JSON.parse(obj));
});

var options = {
  host: Config.db_host,
  port: Config.db_port,
  user: Config.db_username,
  password: Config.db_password,
  database: Config.db_database,
  checkExpirationInterval: 900000,
  expiration: 86400000,
  createDatabaseTable: true,
  schema: {
    tableName: 'sessions',
    columnNames: {
      session_id: 'session_id',
      expires: 'expires',
      data: 'data'
    }
  }
};

var sessionStore = new MySQLStore(options);

app.use(session({
  key: 'session_cookie_name',
  secret: 'session_cookie_secret',
  store: sessionStore,
  resave: true,
  saveUninitialized: true,
  secure: false,
}));

app.use(passport.initialize());
app.use(passport.session());

if (Config.is_dev) {
  app.use(require('connect-livereload')({
    port: 35729
  }));
}

app.use(express.static(__dirname + '/Public'))

var server = app.listen(Config.port, function () {
  var host = server.address().address
  var port = server.address().port

  console.log('Web App is listening at http://%s:%s', host, port)
});

app.get('/login', passport.authenticate('facebook', { scope : 'email' }));

app.get('/facebook_callback', passport.authenticate('facebook', {
  successRedirect: '/',
  failureRedirect: '/404'
}));

app.get('/logout', function(req, res){
  req.logout();
  req.session.destroy(function (err) {
    res.redirect('/');
  });
});

app.post('/api/get_self', function (req, res) {
  if (req.session.passport) {
    var userId = JSON.parse(req.session.passport.user).id
    User.findOne({where: {id: userId}}).then(function (blog) {
      if (blog) {
        res.send(JSON.stringify(blog));
      }
      else {
        res.status(401).send("You are not user in please relogin");
      }
    });
  }
  else {
    res.status(401).send("You are not loggin in");
  }
  // else {
  //   User.findOne({where: {id: 4}, attributes: ['id', 'username', 'roles', 'date', 'facebook'], include: [{ all: true }]}).then(function (blog) {
  //     if (blog) {
  //       res.send(JSON.stringify(blog));
  //     }
  //     else {
  //       res.send(JSON.stringify({error: "You are not loggin in"}));
  //     }
  //   });
  // }
})

app.post('/api/get_blogs', function (req, res) {
  Blog.findAndCountAll({
    order: 'date DESC',
    limit: req.body.limit ? Number(req.body.limit) : 10,
    offset: req.body.skip ? Number(req.body.skip) : 0,
    attributes: ['id', 'title', 'preview', 'date', 'icon', 'image'],
  }).then(function (raw) {
    var data = [];
    _.each(raw.rows, function(row) {
      data.push(row.dataValues)
    });
    res.send(JSON.stringify({
      count: raw.count,
      items: raw.rows,
    }));
  });
})

app.post('/api/get_blog', function (req, res) {
  Blog.findOne({
    where: {id: req.body.id ? Number(req.body.id) : -1},
    attributes: ['id', 'html', 'title', 'preview', 'date', 'icon', 'image'],
  }).then(function (blog) {
    res.send(JSON.stringify(blog));
  });
})

app.post('/api/upsert_blog', function (req, res) {
  if (req.session.passport) {
    var user = JSON.parse(req.session.passport.user)
    if (!_.contains(user.roles, 'admin')) {
      res.status(405).send('You have no right to do this operation')
    }
  }
  else {
    res.status(401).send('Not loggin')
  }

  if (req.body.id == -1) {
    req.body.id = undefined
    Blog.create(req.body).then(function (item) {
      res.send(JSON.stringify(item));
    });
  }
  else {
    Blog.update(req.body, { where: { id: req.body.id }}).then(function (item) {
      res.send(JSON.stringify({id: req.body.id}));
    });
  }
})

app.post('/api/remove_blog', function (req, res) {
  if (req.session.passport) {
    var user = JSON.parse(req.session.passport.user)
    if (!_.contains(user.roles, 'admin')) {
      res.status(405).send('You have no right to do this operation')
    }
  }
  else {
    res.status(401).send('Not loggin')
  }

  Blog.destroy({ where: { id: req.body.id }}).then(function (item) {
    res.send(JSON.stringify(item));
  });
})

app.get('/share/blog/:id', function (req, res) {
  Blog.findOne({
    where: {id: req.params.id ? Number(req.params.id) : -1},
    attributes: ['id', 'html', 'title', 'preview', 'date', 'icon', 'image'],
  }).then(function (blog) {
    var preview = _.map(blog.preview, function(row){
      return _.map(row.items, function(item) {
        return item.text
      }).join()
    }).join('\r\n')

    var Additional = {
      desc: preview,
      title: blog.title,
      image: blog.image.trim() == "" ? (Config.url + "Images/nf.jpg") : blog.image,
      redirrect_to: Config.url + "blog/" + blog.id,
    }

    fs.readFile(__dirname + '/share.html', 'utf8', function(err, text){
      var newText = text.replace(/{{([a-zA-Z0-9_]+)}}/gi, function myFunction(x){
        if (Additional[x.replace(/{{|}}/gi, '')]) {
          return Additional[x.replace(/{{|}}/gi, '')];
        }
        else {
          return Config[x.replace(/{{|}}/gi, '')];
        }
      });
      res.send(newText);
    });
  });
})

app.get('/api/blog/comments/:id', function (req, res) {
  var Additional = {
    desc: "",
    title: "",
    image: "",
    redirrect_to: Config.url + "share/blog/" + req.params.id,
  }

  fs.readFile(__dirname + '/comments.html', 'utf8', function(err, text){
    var newText = text.replace(/{{([a-zA-Z0-9_]+)}}/gi, function myFunction(x){
      if (Additional[x.replace(/{{|}}/gi, '')]) {
        return Additional[x.replace(/{{|}}/gi, '')];
      }
      else {
        return Config[x.replace(/{{|}}/gi, '')];
      }
    });
    res.send(newText);
  });
})

app.get('*', function (req, res) {
  var Additional = {
    description: "Blog about Programming",
    title: "Makame Blog",
    image: "http://makame.ru/Images/5.jpg",
  }

  fs.readFile(__dirname + '/index.html', 'utf8', function(err, text){
    var newText = text.replace(/{{([a-zA-Z0-9_]+)}}/gi, function myFunction(x){
      if (Additional[x.replace(/{{|}}/gi, '')]) {
        return Additional[x.replace(/{{|}}/gi, '')];
      }
      else {
        return Config[x.replace(/{{|}}/gi, '')];
      }
    });
    res.send(newText);
  });
})
